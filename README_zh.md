# 签名代理服务

**签名代理服务(Sign Proxy Service)**，是用于WEB无插件访问USBKEY的客户端代理服务。该服务基于Windows，采用C++开发。

# 开发平台

1. 操作系统： Windows 10 64-bit
2. 开发语言： C++
3. 编译工具： nmake
4. 编辑工具： emacs
5. 网络框架： [libevent-2.0.22-stable][1]
6. Json解析： [jsoncons][2]
7. Hash算法： [hashlib++][3]

# 安装

以管理员权限运行VS命令行提示符，进入工程目录SignProxyService，执行如下命令：

``` shell
# 编译项目
SignProxyService>nmake /F Makefile.nmake
# 清理项目
SignProxyService>nmake /F Makefile.nmake clean
# 只编译SignProxyService
SignProxyService>nmake /F Makefile.nmake sign
# 只清理SignProxyService
SignProxyService>nmake /F Makefile.nmake cleansign
# 安装服务
SignProxyService>src\SignProxyService -i
# 启动服务
SignProxyService>src\SignProxyService -s
# 停止服务
SignProxyService>src\SignProxyService -t
# 卸载服务
SignProxyService>src\SignProxyService -u
```

# 参数说明

| 选项 | 说明 |
|-----|------|
| -h/--help | 显示帮助信息 |
| -i/--install | 安装服务，需要管理员权限 |
| -u/--uninstall | 卸载服务，需要管理员权限 |
| -s/--start | 运行服务，命令行中需要管理员权限 |
| -t/--stop | 停止服务，命令行中需要管理员权限 |


[1]: http://libevent.org/
[2]: https://github.com/danielaparker/jsoncons
[3]: http://hashlib2plus.sourceforge.net/index.html
[4]: http://git.oschina.net/proxy/SignProxyService

