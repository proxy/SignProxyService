// GlobDef.h: Global reference types and constans.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLOBDEF_H__341DFEBD_112D_46C4_809A_096A1117AC3B__INCLUDED_)
#define AFX_GLOBDEF_H__341DFEBD_112D_46C4_809A_096A1117AC3B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <memory.h>

#ifndef NULL
#define NULL               0
#endif // #ifndef NULL

#ifdef _WIN32

#undef far
#undef near
#undef FAR
#undef NEAR

#define far
#define near
#define FAR                 far
#define NEAR                near

#endif // #ifdef _WIN32


#ifndef _WINDEF_

#ifndef CONST
#define CONST               const
#endif // #ifndef CONST

#ifndef DWORD
typedef unsigned long       DWORD;
#endif // #ifndef DWORD

#ifndef PDWORD
typedef DWORD near          *PDWORD;
#endif // #ifndef PDWORD

#ifndef LPDWORD
typedef DWORD far           *LPDWORD;
#endif // #ifndef LPDWORD

#ifndef WORD
typedef unsigned short      WORD;
#endif // #ifndef WORD

#ifndef PWORD
typedef WORD near           *PWORD;
#endif // #ifndef PWORD

#ifndef LPWORD
typedef WORD far            *LPWORD;
#endif // #ifndef LPWORD

#ifndef BYTE
typedef unsigned char       BYTE;
#endif // #ifndef BYTE

#ifndef PBYTE
typedef BYTE near           *PBYTE;
#endif // #ifndef PBYTE

#ifndef LPBYTE
typedef BYTE far            *LPBYTE;
#endif // #ifndef LPBYTE

/* Type define BOOL */
#ifndef BOOL
typedef int                 BOOL;
#endif // #ifndef BOOL

#ifndef PBOOL
typedef BOOL near           *PBOOL;
#endif // #ifndef PBOOL

#ifndef LPBOOL
typedef BOOL far            *LPBOOL;
#endif // #ifndef LPBOOL

#ifndef FLOAT
typedef float               FLOAT;
#endif // #ifndef FLOAT

#ifndef PFLOAT
typedef FLOAT               *PFLOAT;
#endif // #ifndef PFLOAT

#ifndef INT
typedef int                 INT;
#endif // #ifndef INT

#ifndef PINT
typedef int near            *PINT;
#endif // #ifndef PINT

#ifndef PINT
typedef int far             *LPINT;
#endif // #ifndef PINT

#ifndef LPLONG
typedef long far            *LPLONG;
#endif // #ifndef LPLONG

#ifndef LPVOID
typedef void far            *LPVOID;
#endif // #ifndef LPVOID

#ifndef LPCVOID
typedef CONST void far      *LPCVOID;
#endif // #ifndef LPCVOID

#ifndef UINT
typedef unsigned int        UINT;
#endif // #ifndef UINT

#ifndef PUINT
typedef unsigned int        *PUINT;
#endif // #ifndef PUINT

#ifndef INT8
typedef signed char         INT8;
#endif // #ifndef INT8

#ifndef PINT8
typedef signed char         *PINT8;
#endif // #ifndef PINT8

#ifndef INT16
typedef signed short        INT16;
#endif // ifndef INT16

#ifndef PINT16
typedef signed short        *PINT16;
#endif // ifndef PINT16

#ifndef INT32
typedef signed long         INT32;
#endif // ifndef INT32

#ifndef PINT32
typedef signed long         *PINT32;
#endif // ifndef PINT32

#ifndef UINT8
typedef unsigned char       UINT8;
#endif // #ifndef UINT8

#ifndef PUINT8
typedef unsigned char       *PUINT8;
#endif // #ifndef PUINT8

#ifndef UINT16
typedef unsigned short      UINT16;
#endif // ifndef UINT16

#ifndef PUINT16
typedef unsigned short      *PUINT16;
#endif // ifndef PUINT16

#ifndef UINT32
typedef unsigned long       UINT32;
#endif // ifndef UINT32

#ifndef PUINT32
typedef unsigned long       *PUINT32;
#endif // ifndef PUINT32

#endif // #ifndef _WINDEF_

typedef BYTE				*POINTER;
typedef DWORD				RETVAL;

#ifdef _WINBASE_

//#define strcpy(strDestination, strSource)	lstrcpy(strDestination, strSource)
//#define strcmp(strDestination, strSource)	lstrcmp(strDestination, strSource)
//#define strlen(string)						lstrlen(string)

#endif // #ifdef _WINBASE_

#ifndef S_OK
#define S_OK				0x00000000
#endif

//Error codes.
CONST RETVAL RE_DATA				=	0x08040001;
CONST RETVAL RE_DATA_LEN			=	0x08040002;
CONST RETVAL RE_MODULUS_LEN			=	0x08040003;

CONST RETVAL RE_RANDOM_GENERATE			=	0x08040101;
CONST RETVAL RE_RSA_KEYPAIRS_GENERATE	=	0x08040102;
CONST RETVAL RE_RSA_KEYPAIRS_CHECK		=	0x08040103;

CONST RETVAL RE_DIGEST_ALGORITHM	=	0x08040201;
CONST RETVAL RE_CIPHER_ALGORITHM	=	0x08040202;
CONST RETVAL RE_CODING_ALGORITHM	=	0x08040203;
CONST RETVAL RE_RANDOM_ALGORITHM	=	0x08040204;

CONST RETVAL RE_PEM_ENCODING		=	0x08040301;
CONST RETVAL RE_PRV_KEY_ENCODING	=	0x08040302;
CONST RETVAL RE_PUB_KEY_ENCODING	=	0x08040303;
CONST RETVAL RE_PRV_KEY_PASSWORD	=	0x08040304;

CONST RETVAL RE_PRV_KEY_READ		=	0x08040401;
CONST RETVAL RE_PRV_KEY_WRITE		=	0x08040402;
CONST RETVAL RE_PUB_KEY_READ		=	0x08040403;
CONST RETVAL RE_PUB_KEY_WRITE		=	0x08040404;
CONST RETVAL RE_CARD_PASSWORD		=	0x08040405;

CONST RETVAL RE_ENVELOPE_SEAL		=	0x08040501;
CONST RETVAL RE_ENVELOPE_OPEN		=	0x08040502;
CONST RETVAL RE_SIGNATURE_SIGN		=	0x08040503;
CONST RETVAL RE_SIGNATURE_READ		=	0x08040504;
CONST RETVAL RE_SIGNATURE_VERIFY	=	0x08040505;
CONST RETVAL RE_DIGEST				=	0x08040506;
CONST RETVAL RE_BUFFER_OVERLAP		=	0x08040507;

#endif // !defined(AFX_GLOBDEF_H__341DFEBD_112D_46C4_809A_096A1117AC3B__INCLUDED_)
