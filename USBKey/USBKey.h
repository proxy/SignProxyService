 /* eSign project
  * file   : USBKey.h
  * author : Feimyy && wutao Revision
  * Desc   : USBKey签名接口,基于PKCS11实现
  */





#pragma once
#include "global.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <PKCS11\cryptoki.h>
#include "Device.h"
#include "Hash.h"
#ifdef WIN32

#define MOUDLE_HANDLE HMODULE
#include <windows.h>
#define dll_load(library_name,mode) LoadLibrary(library_name)
#define dll_GetProcAddress(library_handle,function_name) GetProcAddress(library_handle,function_name)
#define dll_unload(p) FreeLibrary(p)

#include <iostream>
using namespace std;
#else 
#include <dlfcn.h>
#endif

namespace eSign {
class USBKey : public Device
{
public:

		USBKey();
		/**
		 *  @brief  获取USBKey设备序列号(string需要在外部销毁) 
		 *  @author 2015/12/21 by wutao
		 *  @exception 函数可能会抛出以下异常
		 */

		std::string* GetDeviceID(void);

		/**
		 *  @brief  用户设备初始化,覆盖基类虚函数 
		 *  @param [in] name 用户设备名称，USBKey设备名称为"USBKey"
		 *  @author 2014/4/14 by wutao
		 *  @exception 函数可能会抛出以下异常
		 *  @par 异常说明
		 *  - ERR_PKCS_LOAD_LIB 载入PKCS11库失败
		 *  - ERR_PKCS_GET_FUNCTIONLIST 获取PKCS11库函数列表失败
		 *  - ERR_PKCS_INITIALIZE 初始化用户设备失败
		 */

		 bool Initialize(const char *name);
		/**
		 *  @brief  签名数据
		 *  @param [in] type 使用的Hash算法类型
		 *  @par可用类型如下
		 *  - SIGN_SHA1 使用SHA1算法
		 *  - SIGN_MD5  使用MD5算法
		 *  @param [in] data  需要签名的数据
		 *  @param [in] data_len 签名数据的长度
		 *  @param [in,out] signature 生成的签名值
		 *  @param [in,out] signature_len 生成签名值的长度
		 *  @waring 调用者最好将signature_len长度附一个初值，大小为signature长度，并将signature清空，不然偶尔发发生签名错误
		 *  @author 2014/4/14 by wutao
		 *  @exception 函数可能会抛出以下异常
		 *  @par 异常说明
		 *  - ERR_PKCS_FIND_PRIVATE 查找设备私钥失败
		 *  - ERR_PKCS_NO_SIGN_CERTIFICATE 查找设备签名证书失败
		 *  - ERR_PKCS_SIGNINIT 签名初始失败
		 *  - ERR_HASH_INVALID_MD  无效的HASH类型
		 */

		virtual void SignData(RSA_SIGN_TYPE type,const void *data , unsigned int data_len, unsigned char * signature ,unsigned int &signature_len); 
	
		/**
		 *  @brief  使用USBKey pin登录
		 *  @author 2015/12/24 by wutao
		 *  @param [in]  password 密码
		 *  @param [in]  len  密码长度
		 *  @exception 函数可能会抛出以下异常
		 *  @par 异常说明
		 *  - ERR_PKCS_LOGIN_USBKEY_FAILED 密码错误
		 */
	     bool Login(const  char * password ,int len);

		/**
		 *  @brief  获取设备X509证书，覆盖基类函数
		 *  @author 2014/4/14 by wutao
		 *  @exception 函数可能会抛出以下异常
		 *  @par 异常说明
		 *  - ERR_PKCS_GET_CERTIFICATE 获取证书失败
		 */
		virtual std::string* GetX509();
		virtual ~USBKey(void); //call :finalize
	
public:
		virtual void GetX509(X509 **x) const;
		virtual void GetX509(const X509 **x) const;
		virtual void GetX509(unsigned char *buf, unsigned int &length)const;
		virtual void Sign(RSA_SIGN_TYPE type,const unsigned char * data ,unsigned int data_len, unsigned char * signature , unsigned int & signature_len); 
		virtual bool IsOK() const;
		virtual bool IsLogin() const;
private:
		string m_cert;
		string m_deviceID;
		CK_RV m_rv;
		CK_SLOT_ID_PTR m_slot_list_ptr;
		CK_SLOT_ID m_valid_slot_list[128]; //可用的插槽的列表,保存插入了USBKey的插槽
		CK_ULONG m_valid_slot_count;
		CK_ULONG m_slot_list_count;  // the count of the usbkey slot
		CK_SESSION_HANDLE m_session_handle; // USBKey session handle 
		CK_FUNCTION_LIST_PTR m_Function_List;  //PK11 function list
		void  * m_pk11_lib_handle; //hdpkcs11_jit.dll handle
		bool    m_is_ok;  //The flag of USBKey initialize correct
		bool    m_is_login;  // 是否登录
		bool    m_is_initialize; // The flag of PKCS11 lib initialize correct
		FILE * m_fp; //异常记录文件句柄
		char m_ErrInfo[255]; //异常信息
	
private:


		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      初始化设备(交互态)
		 *  Param  [in]   null_ptr: 通常为NULL_PTR
		 *  return :      true:初始化成功,false:初始化失败
		 *  Caller :      Initialize()
		 ************************************************************************/
		bool initialize(CK_VOID_PTR null_ptr);
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      得到usbkey插槽的列表(交互态)
		 *  return :      true:执行成功,false:执行失败
		 *  Caller :      initialize()
		 ************************************************************************/
		bool get_SlotList();
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      判断USBKey是否存在签名公钥(交互态)
		 *  Param  :[in]  slot的ID
		 *  return :      true:存在,false:不存在
		 *  Caller :      SignData(),get_SlotList()
		 ************************************************************************/	
		 bool get_MechanismInfo(CK_SLOT_ID slot_id);
		 
		 /***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      打开会话(交互态)
		 *  return :      true:打开成功,false:打开失败
		 *  Caller :      initialize()
		 ************************************************************************/	
		bool open_Session();
		

		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      关闭一个打开的会话(交互态)
		 *  return :      true:关闭成功,false:关闭失败
		 *  Caller :      Logout(),open_Session,
		 ************************************************************************/	
		bool close_Session();

		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      登录USBKey(交互态)
		 *  Param  [in]   password: USBKey用户密码
		 *  Param  [in]   password_len: USBKey用户密码长度
		 *  return :      true:登录成功,false:登录失败
		 *  Caller :      Login()
		 ************************************************************************/			
		bool login(CK_UTF8CHAR_PTR password ,CK_ULONG password_len);

		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      注销登录USBKey(交互态)
		 *  Param  [in]   session_handle: USBKey会话句柄
		 *  return :      true:登录成功,false:登录失败
		 *  Caller :      Logout()
		 ************************************************************************/		
		bool logout(CK_SESSION_HANDLE session_handle);
		
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      寻找签名私钥对象(交互态)
		 *  Param  [in]   session_handle: USBKey会话句柄
		 *  Param  [out]   object_handle_ptr: 私钥对象句柄
		 *  return :      CKR_OK:成功,否则失败
		 *  Caller :      SignData()
		 *  Call   :      findObject()
		 ************************************************************************/
		CK_RV find_private_key_object(CK_SESSION_HANDLE session_handle,CK_OBJECT_HANDLE_PTR object_handle_ptr);
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      寻找签名公钥对象(交互态)
		 *  Param  [in]   session_handle: USBKey会话句柄
		 *  Param  [out]  object_handle_ptr: 公钥对象句柄
		 *  return :      CKR_OK:成功,否则失败
		 *  Call   :      findObject()
		 ************************************************************************/		
		
		CK_RV find_public_key_object(CK_SESSION_HANDLE session_handle,CK_OBJECT_HANDLE_PTR object_handle_ptr);
		
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      寻找签名证书对象(交互态)
		 *  Param  [in]   session_handle: USBKey会话句柄
		 *  Param  [out]  object_handle_ptr: 证书对象句柄
		 *  return :      CKR_OK:成功,否则失败
		 *  Caller :      GetX509()
		 *  Call   :      find_object()
		 ************************************************************************/		
		CK_RV find_x509_cert(CK_SESSION_HANDLE session_handle,CK_OBJECT_HANDLE_PTR object_handle_ptr);
		
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      得到签名证书(交互态)
		 *  Param  [in]   session_handle: USBKey会话句柄
		 *  Param  [in]   object_handle_ptr: 证书对象句柄
		 *  return :      true:成功,false:失败
		 *  Caller :      GetX509()
		 ************************************************************************/
		bool get_x509_cert(CK_SESSION_HANDLE session_handle,CK_OBJECT_HANDLE_PTR object_handle_ptr);
		
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      查询对象(交互态)
		 *  Param  [in]   session_handle: USBKey会话句柄
		 *  Param  [in]   object_attribute: 对象属性句柄
		 *  Param  [in]   attribute_count: 属性个数
		 *  Param  [in]   object_attribute: 对象属性句柄
		 *  Param  [out]  object_handle_ptr: 对象句柄
		 *  Param  [out]   object_count: 对象个数
		 *  return :      CKR_OK:成功,否则失败
		 *  Caller :      findPrivate_key(),find_x509_cert()
		 ************************************************************************/
		CK_RV find_object(CK_SESSION_HANDLE session_handle,
						  CK_ATTRIBUTE_PTR object_attribute, 
						  CK_ULONG attribute_count ,
						  CK_OBJECT_HANDLE_PTR object_handle_ptr ,
						  CK_ULONG_PTR object_count_ptr);

		
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      退出登录(用户态)
		 *  Param  [in]   password: USBKey密码
		 *  Param  [in]   password_len: 密码长度
		 *  return :      true:成功,false:失败
		 *  Call   :      logout()
		 ************************************************************************/
		bool Logout();
		
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      退出PK11并卸载pk11链接库句柄(交互态)
		 ************************************************************************/
		void  finalize();
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      获取插槽个数(交互态)
		 *  return :      插槽个数
		 ************************************************************************/
		CK_ULONG get_SlotList_Count();
		
		/***********************************************************************
		 *
         *  Author :      Feimyy		 
		 *  Brief  :      对hash值进行重编码用于签名(交互态)
		 *  Param  [in]   digest: 原始hash值
		 *  Param  [in]   digest_len: hash长度
		 *  Param  [out]  encoded_digest: 编码后的数据
		 *  Param  [out]   encoded_digtst_len: 新数据长度
		 *  return :      true:成功,false:失败
		 *  Caller :      SignData()
		 ************************************************************************/
		bool encode_hash(unsigned char * digest,int digest_len,unsigned char * encoded_digest,int *encoded_digtst_len);
	
};
}

