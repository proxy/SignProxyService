#include <iostream>
#include "global.h"
#include "USBKey.h"
#include "Cert.h"
using namespace std;
using namespace eSign;
size_t  len;
int main() {
	
	cout<<"USBKeyDemo"<<endl;
	//USBKey设备
	USBKey *device = new USBKey;

	try {
		//初始化一个USBKey
		device->Initialize("USBKey");

		//获取USBKey设备是否可用
		bool isok = device->IsOK();
		if(isok) {
			cout<<"USBKey OK"<<endl;
		}
		
		///////////////获得X509证书/////////////////////////////(无需Pin)
		string *x509 = device->GetX509();
		Cert cert(*x509);
		unsigned char certID[256];
		cert.Get_SerialNumber(certID, len);
		cout<<"证书序列号：";
		for(int i=0; i<len; i++) {
			printf("%.2X ", certID[i]);
		}
		cout<<endl;

		//////////////设备序列号///////////（无需Pin）
		string *deviceID = device->GetDeviceID();
		cout<<"设备序列号："<<*deviceID<<endl;

		//////////////////////签名/////////////////////(需要Pin)
		device->Login("12345678", 8);
		string  data("test");   //需要签名的数据 
		unsigned char signeddata[1024]; //签名值
		unsigned int len = 1024;  //签名值长度
		device->SignData(Device::SIGN_SHA1, data.c_str(), data.size(), signeddata, len);
		cout<<"签名值:";
		for(int i=0; i<len; i++){
			printf("%x", signeddata[i]); 
		}
		cout<<endl;
		delete device;
		delete x509;
		delete deviceID;
	} catch(Err &err) {
		cout <<err.what() <<endl;
		return -1;
	}
	

	system("pause");
	
	return 0;
}