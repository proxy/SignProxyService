/* eSign project
 * file : Device.h
 * author : Lodevil
 */

#ifndef __ESIGN_DEVICE_HEADER__
#define __ESIGN_DEVICE_HEADER__

#include "global.h"
#include <string>

namespace eSign
{

#define MAX_DEVICE_LEN 256
/** @class Device
 * @brief 用户加密设备
 * 用于获取用户证书、签名
 */
class Device
{
public:
    enum RSA_SIGN_TYPE
    {
        SIGN_SHA1 = NID_sha1,
        SIGN_MD5 = NID_md5
    };

    Device();
    virtual ~Device();
    /**
     *@brief 初始化设备
     *@param [in] name 设备名称
     *@param [in] password 设备密码
     */
    virtual void Initialize(const char *name, const char *password);
    /**
     *@brief 获取X509格式的证书
     *@param [out] x 复制后的证书地址
     */
    virtual void GetX509(X509 **x) const;
    /**
     *@brief 获取X509格式的证书
     *@param [out] x 证书地址
     */
    virtual void GetX509(const X509 **x) const;
    /**
     *@brief 获取X509格式的证书
     *@param [out] buf 保存证书的地址
     *@param [out] length 证书的长度
     */
    virtual void GetX509(unsigned char *buf, unsigned int &length) const;
    /**
     *@brief 获取X509格式的证书
     *@return 包含证书的string指针（需要在外部销毁）
     */
    virtual const std::string* GetX509();
    /**
    *@brief 直接签名数据
     *@param [in] type 签名类型
     *@param [in] digest 签名的数据
     *@param [in] d_len 签名数据的长度
     *@param [out] sigret 签名结果
     *@param [out] siglen 签名结果长度
     */
    virtual void Sign(RSA_SIGN_TYPE type, const unsigned char *digest,
                      unsigned int d_len, unsigned char *sigret,
                      unsigned int &siglen) ;
    /**
     *@brief 签名数据
     *先求Hash，再签名
     *@param [in] type 签名类型
     *@param [in] msg 签名的数据
     *@param [in] m_len 签名数据的长度
     *@param [out] sigret 签名结果
     *@param [out] siglen 签名结果长度
     */
    virtual void SignData(RSA_SIGN_TYPE type, const void *msg,
                          unsigned int m_len, unsigned char *sigret,
                          unsigned int &siglen) ;
    /*virtual bool encrypt(const void *buf_in, unsigned int length_in,
                unsigned char *buf_out, unsigned int &length_out);
    virtual bool decrypt(const void *buf_in, unsigned int length_in,
                unsigned char *buf_out, unsigned int &length_out);
    virtual bool encrypt(const std::string &buf_in, std::string *buf_out);
    virtual bool decrypt(const std::string &buf_in, std::string *buf_out);*/
    virtual bool IsOK() const;

protected:
    bool is_ok;
    char device[MAX_DEVICE_LEN];			//设备名称
    std::string *x509_str;								//X509格式的证书
};

}
#endif
