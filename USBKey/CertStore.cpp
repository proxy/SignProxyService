/* eSign project
 * file : CertStore.cpp
 * author : Lodevil
 */

#include "CertStore.h"
#include "Cert.h"

namespace eSign {

void CertStore::AddCert(X509 *x509) {
    if (X509_STORE_add_cert(_store, x509) != 1)
        LTHROW(ERR_CERT_STORE_ADD_CERT)
}

void CertStore::AddCert(Cert &cert) {
    if (X509_STORE_add_cert(_store, cert.get_x509()) != 1)
        LTHROW(ERR_CERT_STORE_ADD_CERT)
}

void CertStore::AddCRL(CertCRL &crl) {
    if (X509_STORE_add_crl(_store, crl.get_crl()) != 1)
        LTHROW(ERR_CERT_STORE_ADD_CRL)
}

void CertStore::SetCheckCRL() {
     X509_STORE_set_flags(_store,
        X509_V_FLAG_CRL_CHECK | X509_V_FLAG_CRL_CHECK_ALL);
}

X509_STORE * CertStore::get_store() {
    return _store;
}

};