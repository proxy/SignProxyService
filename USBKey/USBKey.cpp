#include "USBKey.h"
#include "Windows.h"
using namespace eSign;
#ifdef _DEBUG
#define IsLog true
#else
#define IsLog false
#endif
USBKey::USBKey()
{
    this->m_is_ok = false;
    this->m_is_initialize = false;
    this->m_valid_slot_count = 0;
    this->m_slot_list_count = 0;
}


USBKey::~USBKey(void)
{
    if(this->m_is_ok)
    {
        Logout(); //只有在登录成功后才需要退出登录
    }
    if (this->m_is_initialize)
    {
        finalize();  //只有成功初始化才反初始化
    }
}


void USBKey::finalize()
{
    if(m_Function_List != NULL)
    {
        m_Function_List->C_Finalize(NULL_PTR);
        dll_unload((MOUDLE_HANDLE)m_pk11_lib_handle);
    }
}


bool USBKey::login(CK_UTF8CHAR_PTR password ,CK_ULONG password_len)
{
    bool is_ok = true;
    m_rv = m_Function_List->C_Login(m_session_handle,CKU_USER,password,password_len);
	m_rv = CKR_OK;
    if(m_rv != CKR_OK)
    {
        is_ok  = false;
    }

    return is_ok;

}


bool USBKey::open_Session()
{
    bool is_ok = false;
    for(int i = 0; i<m_valid_slot_count; i++)
    {
        m_rv = m_Function_List->C_OpenSession(m_valid_slot_list[i],CKF_SERIAL_SESSION,NULL_PTR,NULL,&m_session_handle);
        if(m_rv != CKR_OK)
        {
            is_ok = false;
            this->close_Session();
        }
        else
        {
            is_ok = true;
            break;
        }
    }
    return is_ok;
}


bool USBKey::close_Session()
{
    if(m_session_handle != NULL && m_Function_List != NULL)
    {
        m_Function_List ->C_CloseSession(m_session_handle);
    }
    return true;
}


CK_RV USBKey::find_object(CK_SESSION_HANDLE session_handle,
                          CK_ATTRIBUTE_PTR object_attribute,
                          CK_ULONG attribute_count ,
                          CK_OBJECT_HANDLE_PTR object_handle_ptr ,
                          CK_ULONG_PTR object_count_ptr
                         )
{
    CK_RV return_rv;

    return_rv = m_Function_List->C_FindObjectsInit(m_session_handle,object_attribute,attribute_count);
    if(return_rv != CKR_OK)
    {
        return return_rv ;
    }
    return_rv = m_Function_List->C_FindObjects(m_session_handle,object_handle_ptr,1,object_count_ptr);
    if(return_rv == CKR_OK && *object_count_ptr != 0)
    {

    }

    return_rv = m_Function_List->C_FindObjectsFinal(m_session_handle);
    if(return_rv != CKR_OK)
    {
        return return_rv ;
    }

    if(*object_count_ptr == 0)
    {
        return CKR_FUNCTION_FAILED;
    }
    return return_rv;

}


CK_RV USBKey::find_private_key_object(CK_SESSION_HANDLE session_handle,CK_OBJECT_HANDLE_PTR object_handle_ptr)
{
    CK_OBJECT_CLASS object_class  = CKO_PRIVATE_KEY;
    CK_BBOOL ck_IsSign = CK_TRUE;
    CK_ATTRIBUTE private_key_template[] =
    {
        {CKA_CLASS,&object_class,sizeof(object_class)},
        {CKA_SIGN,&ck_IsSign,sizeof(ck_IsSign)}
    };

    object_class = CKO_PRIVATE_KEY;
    CK_ULONG attribute_count;
    CK_ULONG object_count;
    object_count = 1;
    attribute_count = sizeof(private_key_template)/sizeof(CK_ATTRIBUTE);
    return find_object(m_session_handle,private_key_template,attribute_count,object_handle_ptr,&object_count);
}


CK_RV USBKey::find_public_key_object(CK_SESSION_HANDLE session_handle,CK_OBJECT_HANDLE_PTR object_handle_ptr)
{
    CK_OBJECT_CLASS object_class  = CKO_PUBLIC_KEY;
    CK_BBOOL ck_IsSign = CK_TRUE;
    CK_ATTRIBUTE public_key_template[] =
    {
        {CKA_CLASS,&object_class,sizeof(object_class)},
        {CKA_SIGN,&ck_IsSign,sizeof(ck_IsSign)}
    };

    object_class = CKO_PUBLIC_KEY;
    CK_ULONG attribute_count;
    CK_ULONG object_count;
    object_count = 1;
    attribute_count = sizeof(public_key_template)/sizeof(CK_ATTRIBUTE);

    return find_object(m_session_handle,public_key_template,attribute_count,object_handle_ptr,&object_count);
}


bool USBKey::get_SlotList()
{
    bool is_ok = true ;
    m_rv = m_Function_List->C_GetSlotList(CK_FALSE,m_slot_list_ptr,&m_slot_list_count); //获取插槽的个数
    if(m_rv != CKR_OK)
    {
        is_ok  = false;
    }
    /*
      得到可用的solt ID
    */
    CK_SLOT_INFO slot_Info;
    for(int i = 0; i<m_slot_list_count; i++)
    {
        m_rv = m_Function_List->C_GetSlotInfo(*(m_slot_list_ptr+i),&slot_Info); //判断Slot是否可用
        if(m_rv == CKR_OK)
        {
            if(get_MechanismInfo(*(m_slot_list_ptr+i)))
            {
                m_valid_slot_list[m_valid_slot_count] = *(m_slot_list_ptr + i);  //保存可用的slot id
                m_valid_slot_count++;   //可用的插槽个数自加上
            }
            else
            {
                continue;
            }
        }
        else
        {
            continue;
        }
    }

	//获取TokenInfo
	CK_TOKEN_INFO tokenInfo;
	m_Function_List->C_GetTokenInfo(m_valid_slot_list[0], &tokenInfo);
	strncpy((char*)m_deviceID.c_str(), (char*)tokenInfo.serialNumber, 16);

    if(m_valid_slot_count == 0 )
    {
        is_ok  = false;
        LTHROW(ERR_PKCS_NODEVICE)
    }

    return is_ok;
}

bool USBKey::Initialize(const char *name)
{
    CK_RV (* __C_GetFunctionList)(CK_FUNCTION_LIST_PTR_PTR Function_List_PTR_PTR);

//    this->m_pk11_lib_handle = dll_load("hdpkcs11_jit.dll",RTLD_LAZY);  //山东CA PKCS11库
  //  this->m_pk11_lib_handle = dll_load("SccaCSP11.dll",RTLD_LAZY);   //四川CA PKCS11库
	this->m_pk11_lib_handle = dll_load("SCCAP11V211.dll",RTLD_LAZY ); //飞天Key
   // this->m_pk11_lib_handle = dll_load("ahcaftp11d.dll",RTLD_LAZY);  //安徽CA PKCS11库
    if(this->m_pk11_lib_handle == 0)
    {
        LTHROW(ERR_PKCS_LOAD_LIB)
        return false;
    }

    __C_GetFunctionList = (CK_RV (*) (CK_FUNCTION_LIST_PTR_PTR Function_List_PTR_PTR))
                          dll_GetProcAddress((MOUDLE_HANDLE)m_pk11_lib_handle,"C_GetFunctionList");
    if(__C_GetFunctionList == 0)
    {
        dll_unload((MOUDLE_HANDLE)m_pk11_lib_handle);
        LTHROW(ERR_PKCS_GET_FUNCTIONLIST)
        return false;
    }

    m_rv = __C_GetFunctionList(&m_Function_List);
    if(m_rv != CKR_OK)
    {
        dll_unload((MOUDLE_HANDLE)m_pk11_lib_handle);
        LTHROW(ERR_PKCS_GET_FUNCTIONLIST)
    }

    if(initialize(NULL_PTR))
    {
        this->m_is_ok = true;
        this->m_is_initialize = true;
    }
    else
    {
        this->finalize();
        LTHROW(ERR_PKCS_INITIALIZE)
    }
	return true;
}

bool USBKey::initialize(CK_VOID_PTR null_ptr)
{
    bool is_ok = true;
    m_rv = m_Function_List->C_Initialize(null_ptr);
    if(m_rv == CKR_OK )
    {
        m_is_initialize = true;

    }
    else
    {
        if(m_rv != CKR_CRYPTOKI_ALREADY_INITIALIZED)
        {
            this->finalize();
            is_ok = false;
            return is_ok;
        }
        else
        {
            m_is_initialize = true;
        }
    }

	//申请空间
    m_slot_list_count = get_SlotList_Count();
    m_slot_list_ptr = new CK_SLOT_ID[m_slot_list_count];

    if( !get_SlotList())
    {
        is_ok = false;
    }

    if(!open_Session())
    {
        is_ok = false;
    }
    return is_ok;

}


bool USBKey::logout(CK_SESSION_HANDLE session_handle)
{
    if(m_Function_List != NULL && this->m_session_handle != NULL)
    {
        m_Function_List->C_Logout(session_handle);

    }
    return true;
}


bool USBKey::Logout()
{
    if(m_Function_List != NULL && this->m_session_handle != NULL)
    {
        this->close_Session();
        this->logout(m_session_handle);
    }
    return true;
}


void USBKey::SignData(RSA_SIGN_TYPE type,const void *data , unsigned int data_len, unsigned char * signature ,unsigned int &signature_len)
{
    CK_OBJECT_HANDLE private_key_object_handle;
    CK_MECHANISM hash_type;
    hash_type.mechanism = CKM_RSA_PKCS;
    hash_type.pParameter = NULL_PTR;
    hash_type.ulParameterLen = 0;

    CK_RV rv = find_private_key_object(m_session_handle,&private_key_object_handle);
    if(rv != CKR_OK)
    {
        LTHROW(ERR_PKCS_FIND_PRIVATE)
    }

    int i = 0;
    for(i = 0; i < m_valid_slot_count; i++)
    {
        if(get_MechanismInfo(m_valid_slot_list[i]))
        {
            break;
        }
    }
    if(i == m_valid_slot_count)
    {
        LTHROW(ERR_PKCS_NO_SIGN_CERTIFICATE)
    }

    rv = m_Function_List->C_SignInit(m_session_handle,&hash_type,private_key_object_handle);
    if(rv != CKR_OK)
    {
        LTHROW(ERR_PKCS_SIGNINIT)
    }

    unsigned char digest[64];
    int d_len;

    switch(type)
    {
    case SIGN_SHA1:
        Hash::SHA1(data, data_len, digest);
        d_len = 20;
        break;
    case SIGN_MD5:
        Hash::MD5(data, data_len, digest);
        d_len = 16;
        break;
    default:
        LTHROW(ERR_HASH_INVALID_MD)
    }

    CK_ULONG CK_signature_len;

    unsigned char encoded_digest[254];
    int encoded_digest_len;
    encode_hash((unsigned char *)&digest,d_len,encoded_digest,&encoded_digest_len);

    /* 签名 */
    CK_signature_len = signature_len;
    rv = m_Function_List->C_Sign(m_session_handle,(CK_BYTE_PTR)encoded_digest,(CK_ULONG)encoded_digest_len,(CK_BYTE_PTR)signature,&CK_signature_len);
    if(rv != CKR_OK)
    {
        LTHROW(ERR_PKCS_SIGN)
    }
    signature_len = (unsigned int )CK_signature_len;
}


CK_ULONG USBKey::get_SlotList_Count()
{
    CK_ULONG slot_list_count;
    m_Function_List->C_GetSlotList(CK_FALSE,NULL_PTR,&slot_list_count);
    return slot_list_count;

}


bool USBKey::Login(const  char * password ,int password_len)
{
    bool is_ok = true ;
    CK_UTF8CHAR_PTR CK_password = (CK_UTF8CHAR_PTR)password;
    CK_ULONG CK_password_len = (CK_ULONG) password_len;
    if(!login(CK_password,CK_password_len))
    {
        is_ok = false;
        this->Logout();
    }
    delete [] m_slot_list_ptr;
	if(!is_ok) {
		 LTHROW(ERR_PKCS_LOGIN_USBKEY_FAILED)
	}
    return is_ok;
}


bool USBKey::get_MechanismInfo(CK_SLOT_ID slot_id)
{
    bool is_ok = false;
    CK_MECHANISM_INFO mechamism_info;
    CK_RV rv = m_Function_List->C_GetMechanismInfo(slot_id,CKM_RSA_PKCS,&mechamism_info);
    if(rv != CKR_OK)
    {
        is_ok = false;
    }
    else
    {
        if(mechamism_info.flags & CKF_SIGN )
        {
            is_ok = true;
        }
    }
    return is_ok;
}


CK_RV USBKey::find_x509_cert(CK_SESSION_HANDLE session_handle,CK_OBJECT_HANDLE_PTR object_handle_ptr)
{
    CK_OBJECT_CLASS object_class  = CKO_CERTIFICATE;
    CK_ATTRIBUTE ceritificate_template[] =
    {
        {CKA_CLASS,&object_class,sizeof(object_class)},
    };

    object_class = CKO_CERTIFICATE;
    CK_ULONG attribute_count;
    CK_ULONG object_count;
    object_count = 1;
    attribute_count = sizeof(ceritificate_template)/sizeof(CK_ATTRIBUTE);
    m_rv = find_object(m_session_handle,ceritificate_template,attribute_count,object_handle_ptr,&object_count);
    return m_rv;
}


bool USBKey::get_x509_cert(CK_SESSION_HANDLE session_handle,CK_OBJECT_HANDLE_PTR object_handle_ptr)
{
    bool is_ok = true;

    CK_ATTRIBUTE ceritificate_pulic_template[] =
    {
        {CKA_CLASS,NULL,0},
        {CKA_VALUE,NULL,0}

    };
    m_rv = m_Function_List->C_GetAttributeValue(m_session_handle,*object_handle_ptr,ceritificate_pulic_template,2);
    if(m_rv != CKR_OK)
    {
        is_ok = false;
    }
    ceritificate_pulic_template[0].pValue = new unsigned char[ceritificate_pulic_template[0].ulValueLen];
    ceritificate_pulic_template[1].pValue = new unsigned char[ceritificate_pulic_template[1].ulValueLen];
    m_rv = m_Function_List->C_GetAttributeValue(m_session_handle,*object_handle_ptr,ceritificate_pulic_template,2);
    if(m_rv != CKR_OK)
    {
        is_ok = false;
    }

    unsigned char * tmp = new unsigned char [ceritificate_pulic_template[1].ulValueLen];
    memcpy(tmp,ceritificate_pulic_template[1].pValue,ceritificate_pulic_template[1].ulValueLen);

    for(int i = 0; i<ceritificate_pulic_template[1].ulValueLen; i++)
    {
        m_cert += tmp[i];
    }
    delete tmp;

    delete ceritificate_pulic_template[0].pValue;
    delete ceritificate_pulic_template[1].pValue;

    return is_ok;


}


std::string*  USBKey::GetX509()
{
    CK_OBJECT_HANDLE object_handle;
    find_x509_cert(m_session_handle,&object_handle);
    if(!get_x509_cert(m_session_handle,&object_handle))
    {
        LTHROW(ERR_PKCS_GET_CERTIFICATE)
    }

    std::string *cert = new std::string;
    *cert = m_cert;
    return cert;
}


bool USBKey::IsOK() const
{
    return m_is_ok;
}
bool USBKey::IsLogin() const
{
	return m_is_login;
}


void USBKey::GetX509(X509 **x) const
{

}


void USBKey::GetX509(const X509 **x) const
{
}


void USBKey::GetX509(unsigned char *buf, unsigned int &length) const
{

}


void USBKey::Sign(RSA_SIGN_TYPE type,const unsigned char * data ,unsigned int data_len, unsigned char * signature , unsigned int & signature_len)
{
    SignData(type,(const void *)data,data_len,signature,signature_len);
}


bool USBKey::encode_hash(unsigned char * digest,int digest_len,unsigned char * encoded_digest,int *encoded_digtst_len)
{
    if (digest_len == 16)
    {
        memcpy(encoded_digest, "\x30\x20\x30\x0c\x06\x08\x2a\x86\x48\x86\xf7\x0d\x02\x05\x05\x00\x04\x10",18);
        memcpy(encoded_digest + 18, (const void *)&digest, digest_len);
        *encoded_digtst_len = digest_len+18;
    }
    else if (digest_len == 20)
    {
        memcpy(encoded_digest, "\x30\x21\x30\x09\x06\x05\x2b\x0e\x03\x02\x1a\x05\x00\x04\x14",15);
        memcpy(encoded_digest + 15, (const void * )digest, digest_len);
        *encoded_digtst_len = digest_len + 15;
    }
    else
    {
        return FALSE;
    }

    return TRUE;
}

std::string* USBKey::GetDeviceID()
{
	string *deviceID = new string(m_deviceID.c_str());
	
	return deviceID;
}

