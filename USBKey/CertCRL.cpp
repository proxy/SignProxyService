/* eSign project
 * file : CertCRL.hpp
 * author : Lodevil
 */
#include "CertCRL.h"
#include "global.h"
using namespace eSign;
CertCRL::CertCRL() {
        _crl = NULL;
    }
CertCRL::~CertCRL() {
        if (_crl) {
            X509_CRL_free(_crl);
        }
    }
    /**
        *@brief Load der format crl
        *@param [in] buf DER address
        *@param [in] size crl length
    */
void CertCRL::LoadDERBuffer(void *buf, size_t size) {
        BIO *bio = BIO_new(BIO_s_mem());
        BIO_write(bio, buf, size);
        d2i_X509_CRL_bio(bio, &_crl);
        BIO_free(bio);
        if (_crl == NULL)
            LTHROW(ERR_CERT_CRL_LOAD)
    }
    /**
        *@brief load crl from der format file
        *@param [in] path crl file path
    */
 void CertCRL::LoadDERFile(const char *path) {
        FILE *f_crl = fopen(path, "r");
        if (!f_crl)
            LTHROW(ERR_CERT_CRL_LOAD)

        d2i_X509_CRL_fp(f_crl, &_crl);
        fclose(f_crl);
        if (_crl == NULL)
            LTHROW(ERR_CERT_CRL_LOAD)
    }
    /**
        *@brief load crl from pem format file
        *@param [in] path crl file path
    */
void CertCRL::LoadPEMFile(const char *path) {
        BIO *bio = BIO_new_file(path, "r");
        _crl = PEM_read_bio_X509_CRL(bio, NULL, NULL, NULL);
        if (_crl == NULL)
            LTHROW(ERR_CERT_CRL_LOAD)
    }

 X509_CRL* CertCRL::get_crl() {
        return _crl;
    }


