/* eSign project
 * file : CertCRL.hpp
 * author : Lodevil
 */

#ifndef __ESIGN_CERTCRL_HPP__
#define __ESIGN_CERTCRL_HPP__

#include "global.h"

namespace eSign {

/** @class CertCRL
 * @brief CRL helpers
 * for CRL
 */
class CertCRL {
public:
    CertCRL();
    ~CertCRL();
    /**
        *@brief Load der format crl
        *@param [in] buf DER address
        *@param [in] size crl length
    */
    void LoadDERBuffer(void *buf, size_t size);
    /**
        *@brief load crl from der format file
        *@param [in] path crl file path
    */
    void LoadDERFile(const char *path);
    /**
        *@brief load crl from pem format file
        *@param [in] path crl file path
    */
    void LoadPEMFile(const char *path);

    X509_CRL* get_crl();
private:
    X509_CRL *_crl;
};

};

#endif
