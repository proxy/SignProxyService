/* eSign project
 * file : Err.cpp
 * author : Lodevil
 */

#include "Err.h"
#include <stdio.h>
using namespace eSign;

static const char *errs[] = {
"no error", //ERR_NOERR
    "*_CTX 初始化失败", //ERR_HASH_CTX_INIT
    "更新哈希数据失败", //ERR_HASH_UPDATE
    "计算哈希值失败", //ERR_HASH_FINAL
    "非法的哈希类型", //ERR_HASH_INVALID_MD
    "载入证书错误!", //ERR_CERT_LOAD
    
    "证书不可用", //ERR_CERT_INVALID
    "得到USBKey公钥失败", //ERR_CERT_PUBKEY
    "公钥验证初始化失败", //ERR_CERT_VERIFY_INIT
    "证书验证更新失败", //ERR_CERT_VERIFY_UPDATE
    "证书验证失败", //ERR_CERT_VERIFY_FINAL
    "载入CRL列表失败", //ERR_CERT_CRL_LOAD_FAIL
    "添加证书存储失败", //ERR_CERT_STORE_ADD_CERT,
    "添加证书吊销列表失败", //ERR_CERT_STORE_ADD_CRL
    
    "用户设备初始化失败", //ERR_DEVICE_INIT
    "用户设备未初始化", //ERR_DEVICE_UNINITED
    "获取用户证书失败", //ERR_DEVICE_CERT
    "USBKey签名失败", //ERR_DEVICE_SIGN
    "设备加密设备", //ERR_DEVICE_ENCRYPT
    "设备解密设备", //ERR_DEVICE_DECRYPT
    "Socket启动失败", //ERR_WSA_STARTUP
    "Socket版本错误", //ERR_SOCKET_VERSION
    
    "创建Socket失败", //ERR_SOCKET
    "连接印章中心失败,请确保已经连接网络或服务器IP地址可用", //ERR_SOCKET_CONN
    "不能建立SSL连接,请先登录", //ERR_SSL_CTX
    "创建SSL失败", //ERR_SSL_NEW
    "不能建立SSL连接,SSL_Connnect 失败", //ERR_SSL_CONN
    "不能建立SSL连接,SSL_read 失败", //ERR_SSL_READ
    "不能建立SSL连接,SSL_write 失败", //ERR_SSL_WRITE
    
    "evp 初始化失败", //ERR_EVP_INIT
    "evp update 失败", //ERR_EVP_UPDATE
    "evp final 失败", //ERR_EVP_FINAL
    
    "seal 数据不完整", //ERR_SEAL_INCOMPLETE
    "seal 非法(signature ..etc.)", //ERR_SEAL_INVALID
    "seal 序列化失败", //ERR_SEAL_SERIALIZE
    "seal 反序列号失败", //ERR_SEAL_PARSE
    "seallist 序列号失败", //ERR_SEALLIST_SERIALIZE
    "seallist 反序列化失败", //ERR_SEALLIST_PARSE
    
    "msg 数据部完整", //ERR_MSG_INCOMPLETE
    "没有设定文档的Hash值", //ERR_MSG_NEED_DOC
    "msg 序列化失败", //ERR_MSG_SERIALIZE
    "ds_hash 非法", //ERR_MSG_DS_INVALID
    
    "文档内容被修改", //ERR_SIG_DOC_INVALID
    "前一个印章无效", //ERR_SIG_PRESIG_INVALID
    "sig 序列化失败", //ERR_SIG_SERIALIZE
    "验证失败,请先验证文档内容", //ERR_SIG_NEED_DOC
    "没有发现任何的签章,您确定文档已经盖过章了吗？", //ERR_SIG_NOITEM
    "sigitem 序列化失败", //ERR_SIGITEM_SERIALIZE
    "sigitem 不完整", //ERR_SIGITEM_INCOMPLETE
    "sigitem 非法", //ERR_SIGITEM_INVALID
    
    "找不到该设备", //ERR_USER_NOSUCH_DEVICE
    "用户未初始化", //ERR_USER_UNINITED
    "用户未连接", //ERR_USER_UNCONNECTED
    "用户未登录", //ERR_USER_UNLOGIN
    "登录印章中心失败,请检查用户名和密码是否正确", //ERR_USER_LOGIN
    "用户发送数据失败", //ERR_USER_SEND
    "用户接收数据失败", //ERR_USER_RECV
    "得到印章列表失败,请检查印章中心是否工作正常或是否拥有签章", //ERR_USER_GETSEALLIST
    "没有可用的印章,请检查印章中心是否工作正常或是否拥有签章", //ERR_USER_GETSEAL
    "不可识别的数据,请检查印章中心是否工作正常", //ERR_USER_SERVER_DATA_INVALID
    
    "未知错误", //ERR_UNKNOWN
    "bio 错误", //ERR_BIO
	"设备验证失败", //ERR_DEVICE_VERIFY
	"未找到USBKey设备", //ERR_PKCS_NODEVICE
	"载入PKCS11库失败", //ERR_PKCS_LOAD_LIB
	"获取 PKCS11 函数列表失败", //ERR_PKCS_GET_FUNCTIONLIST
	"PKCS11初始化失败", //ERR_PKCS_INITIALIZE
	"查找用户私钥失败",  //ERR_PKCS_FIND_PRIVATE
	"查找用户签名证书失败", //ERR_PKCS_NO_SIGN_CERTIFICATE
	"签名初始化失败", //ERR_PKCS_SIGNINIT
	"签名失败", //ERR_PKCS_SIGN
	"获取用户公钥失败", //ERR_PKCS_GET_CERTIFICATE
	"USBKey密码错误", //ERR_PKCS_PASSWORD_INVALID
	"打开USBKey失败，请检查设备密码或者设备是否被系统识别",//ERR_PKCS_LOGIN_USBKEY_FAILED
	"根证书链未初始化", //ERR_PKCS_LOADCHAIN_UNINIT
	"根证书载入失败",//ERR_PKCS_LOADROOT_FAILED
	"根证书链载入失败"//ERR_PKCS_LOADCHAIN_FAILED
};

Err::Err(int errcode, const char *file, int line)
{
    this->errcode = errcode;
    this->line = line;
    this->file = file;
}

const char* Err::what() const throw()
{
    static char msg[100];
    
    if(file && line !=-1){
        sprintf(msg, "FILE:%s,LINE:%d : %s", file, line ,errs[errcode]);
        return msg;
    }
    
    return errs[errcode];
}

int Err::code() const
{
    return errcode;
}














