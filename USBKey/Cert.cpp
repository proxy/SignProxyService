/* eSign project
 * file : Cert.cpp
 * author : Lodevil
 */

#include "Cert.h"

using namespace eSign;

Cert::Cert()
{
    cert = NULL;
    chain = NULL;
}

Cert::Cert(const std::string &buf)
{
    chain = NULL;
    Load(buf.c_str(), buf.size());

}

Cert::Cert(const void *buf, unsigned int length)
{
    chain = NULL;
    Load(buf, length);
}

Cert::Cert(X509 *x509)
{
    chain = NULL;
    cert = X509_dup(x509);
}

Cert::~Cert()
{
    if(cert)
        X509_free(cert);
    if (chain)
        sk_X509_free(chain);
}

void Cert::Load(const void *buf, unsigned int length)
{
    cert = d2i_X509(NULL, (const unsigned char**)&buf, length);
    if(!cert)
        LTHROW(ERR_CERT_LOAD)
    }

void Cert::LoadFile(const char *path)
{
    BIO *bio = BIO_new_file(path, "r");
    if (!bio)
        LTHROW(ERR_CERT_LOAD)
        cert = PEM_read_bio_X509(bio, NULL, NULL, NULL);
    BIO_free(bio);
    if(!cert)
        LTHROW(ERR_CERT_LOAD)
    }

bool Cert::VerifyCert()
{
    X509_STORE *store=NULL;
    X509_STORE_CTX ctx;
    if(!cert)
    {
        LTHROW(ERR_CERT_LOAD)
    }
    //创建X509_store对象，用来存储证书、撤销列表等
    store=X509_STORE_new();
    //设置验证标记，此处设置为全部验证
    X509_STORE_set_flags(store, X509_V_FLAG_CRL_CHECK_ALL);
    //初始化CTX，该类为上下文，此类搜集完必要的信息数据，可以进行验证
    if (!X509_STORE_CTX_init(&ctx, store, cert, NULL))  //最后一个参数为NULL，表示不加载证书吊销列表
    {
        return false;
        goto end;
    }
    if(!chain)
    {
        LTHROW(ERR_PKCS_LOADCHAIN_UNINIT) //证书链条未初始化
    }
    X509_STORE_CTX_trusted_stack(&ctx, chain); //将证书链条加入可信区域
    int ret = X509_verify_cert(&ctx);
end:
    X509_STORE_CTX_cleanup(&ctx);
    if (store)
    {
        X509_STORE_free(store);
    }
    return ret == 1;
}

EVP_PKEY* Cert::Pubkey() const
{
    if(!cert)
        LTHROW(ERR_CERT_INVALID)

        EVP_PKEY *pubkey;
    pubkey = X509_get_pubkey(cert);
    if(pubkey)
        return pubkey;
    LTHROW(ERR_CERT_PUBKEY)
}

void Cert::VerifyInit(HashMD md)
{
    if(!cert)
        LTHROW(ERR_CERT_INVALID)

#define VERIFY_INIT(md)\
    if(EVP_VerifyInit(&cert_verify_md, EVP_##md()) != 1)\
        LTHROW(ERR_CERT_VERIFY_INIT)\
        break;
        switch(md)
        {
        case HASH_MD5:
            VERIFY_INIT(md5)
        case HASH_SHA1:
            VERIFY_INIT(sha1)
        case HASH_SHA256:
            VERIFY_INIT(sha256)
        case HASH_SHA512:
            VERIFY_INIT(sha512)
        default:
            LTHROW(ERR_HASH_INVALID_MD)
        }
}

void Cert::VerifyUpdate(const void *buf, unsigned int length)
{
    if(!cert)
        LTHROW(ERR_CERT_INVALID)

        if(EVP_VerifyUpdate(&cert_verify_md, buf, length) != 1)
            LTHROW(ERR_CERT_VERIFY_UPDATE)
        }

void Cert::VerifyUpdate(const std::string &buf)
{
    return VerifyUpdate(buf.c_str(), buf.size());
}

bool Cert::VerifyFinal(const void *sig, unsigned int length)
{
    if(!cert)
        LTHROW(ERR_CERT_INVALID)

        int err = EVP_VerifyFinal(&cert_verify_md, (const unsigned char*)sig,
                                  length, Pubkey());
    if(err == 1)
        return true;
    else if(err == 0)
        return false;
    LTHROW(ERR_CERT_VERIFY_FINAL)
}

bool Cert::VerifyFinal(const std::string &sig)
{
    return VerifyFinal(sig.c_str(), sig.size());
}

bool Cert::Verify(HashMD md, const void *buf, unsigned int buf_len,
                  const void *sig, unsigned int sig_len)
{
    VerifyInit(md);
    VerifyUpdate(buf, buf_len);
    return VerifyFinal(sig, sig_len);
}

bool Cert::Verify(HashMD md, const std::string &buf, const std::string &sig)
{
    return Verify(md, buf.c_str(), buf.size(), sig.c_str(), sig.size());
}

int Cert::version()
{
    return X509_get_version(cert);
}

const X509_NAME* Cert::issuer()
{
    return X509_get_issuer_name(cert);
}

const X509_NAME* Cert::subject()
{
    return X509_get_subject_name(cert);
}

X509* Cert::get_x509()
{
    return cert;
}

int Cert::Get_SerialNumberLength()
{
    ASN1_INTEGER * SerialNumber = X509_get_serialNumber(cert);
    return SerialNumber->length+2;

}

int Cert::Get_InvalidDateLength()
{
    ASN1_TIME * InvalidDate = X509_get_notAfter(cert);
    return InvalidDate->length+2;
}

int Cert::Get_ValidDateLength()
{
    ASN1_TIME * ValidDate = X509_get_notBefore(cert);
    return ValidDate->length+2;
}

void Cert::Get_CertInvalidDate(OUT unsigned char ** ppUTF8InvalidDate)
{
    if(ppUTF8InvalidDate != NULL)
    {

        ASN1_TIME * InvalidDate = X509_get_notAfter(cert);
        ASN1_STRING_to_UTF8(ppUTF8InvalidDate,InvalidDate);
    }
}
void Cert::Get_CertValidDate(OUT unsigned char ** ppUTF8ValidDate)
{
    if( ppUTF8ValidDate != NULL)
    {
        ASN1_TIME * ValidDate = X509_get_notBefore(cert);
        ASN1_STRING_to_UTF8(ppUTF8ValidDate,ValidDate);
    }
}

void Cert::Get_SerialNumber(OUT unsigned char* pSerialNumber, size_t &len)
{
    if(pSerialNumber != NULL)
    {
        ASN1_INTEGER * SerialNumber = X509_get_serialNumber(cert);
        if(*SerialNumber->data >= 0x80)
        {
            *pSerialNumber = 0x00;
            memcpy_s(pSerialNumber+1, SerialNumber->length, SerialNumber->data, SerialNumber->length);
            len = SerialNumber->length +1;
        }
        else
        {
            memcpy_s(pSerialNumber,SerialNumber->length,SerialNumber->data,SerialNumber->length);
            len = SerialNumber->length;
        }
    }
}
int Cert::Get_derPubKeyLength(IN EVP_PKEY * pPubKey)
{
    return i2d_PublicKey(pPubKey,NULL)+2;
}
void Cert::Get_derPubKey(OUT unsigned char * pderPubKey,IN EVP_PKEY * pPubKey)
{
    i2d_PublicKey(pPubKey,&pderPubKey);
}

int Cert::Get_CertSubjectNum()
{
    X509_NAME * Subject = X509_get_subject_name(cert);
    return X509_NAME_entry_count(Subject);
}

void Cert::Get_CertSujectInfo(OUT unsigned char ** ppSubjectInfo,IN int nIndex,OUT TYPE * enumType)
{
    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);
    if(nIndex <= nNum && ppSubjectInfo!= NULL)
    {

        X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, nIndex);

        ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
        ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);
        ASN1_STRING_to_UTF8(ppSubjectInfo,str);

        int nNid = OBJ_obj2nid(object);

        switch(nNid)
        {
        case NID_countryName:                       // 国家
            *enumType = CountryName;
            break;
        case NID_stateOrProvinceName:               // 省
            *enumType = StateOrProvinceName;
            break;
        case NID_localityName:                      // 地区
            *enumType = LocalityName;
            break;
        case NID_organizationName:                  // 组织
            *enumType = OrganizationName;
            break;
        case NID_organizationalUnitName:            // 单位
            *enumType = OrganizationalUnitName;
            break;
        case NID_commonName:                        // 通用名
            *enumType = CommonName;
            break;
        case NID_pkcs9_emailAddress:                 // Mail
            *enumType = Pkcs9_emailAddress;
            break;
        }

    }
}
int Cert::Get_CertPreSubjectLength(IN int nIndex)
{
    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);
    if(nIndex <= nNum)
    {

        X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, nIndex);

        ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
        ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);
        return str->length;
    }
    return -1;
}

int Cert::Get_IndexOfSubject(IN TYPE enumType)
{
    int nIndex = -1;

    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);
    for(int i =0; i<nNum; i++)
    {

        X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, i);

        ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
        ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);

        int nNid = OBJ_obj2nid(object);

        if(nNid == NID_countryName && enumType == CountryName)
        {
            nIndex = i;
            break;
        }
        if(nNid == NID_commonName && enumType == CommonName )
        {
            nIndex = i;
            break;
        }
        if(nNid == NID_localityName && enumType == LocalityName )
        {
            nIndex = i;
            break;
        }
        if(nNid == NID_organizationalUnitName && enumType == OrganizationalUnitName )
        {
            nIndex = i;
            break;
        }
        if(nNid == NID_organizationName && enumType == OrganizationName )
        {
            nIndex = i;
            break;
        }
        if(nNid == NID_stateOrProvinceName && enumType == StateOrProvinceName )
        {
            nIndex = i;
            break;
        }

    }

    return nIndex;
}

int Cert::Get_LengthOfSubject(IN TYPE enumType)
{
    int nLength = 0;

    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);
    for(int i =0; i<nNum; i++)
    {

        X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, i);

        ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
        ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);

        int nNid = OBJ_obj2nid(object);

        if(nNid == NID_countryName && enumType == CountryName)
        {
            nLength = str->length + 2;
            break;
        }
        if(nNid == NID_commonName && enumType == CommonName )
        {
            nLength = str->length + 2;
            break;
        }
        if(nNid == NID_localityName && enumType == LocalityName )
        {
            nLength = str->length + 2;
            break;
        }
        if(nNid == NID_organizationalUnitName && enumType == OrganizationalUnitName )
        {
            nLength = str->length + 2;
            break;
        }
        if(nNid == NID_organizationName && enumType == OrganizationName )
        {
            nLength = str->length + 2;
            break;
        }
        if(nNid == NID_stateOrProvinceName && enumType == StateOrProvinceName )
        {
            nLength = str->length + 2;
            break;
        }

    }

    return nLength;
}

bool Cert::Get_CoutryName(OUT unsigned char ** ppUTF8CoutryName ,IN int nIndex)
{

    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);

    if(nIndex == -1 || nIndex < 0 || nIndex >= nNum)
    {
        return false;
    }
    X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, nIndex);

    ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
    ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);
    ASN1_STRING_to_UTF8(ppUTF8CoutryName,str);
    return true;
}

bool Cert::Get_StateOrProvinceName(OUT unsigned char ** ppUTF8StateOrProvinceName ,IN int nIndex)
{

    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);

    if(nIndex == -1 || nIndex < 0 || nIndex >= nNum)
    {
        return false;
    }
    X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, nIndex);

    ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
    ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);
    ASN1_STRING_to_UTF8(ppUTF8StateOrProvinceName,str);
    return true;
}
bool Cert::Get_LocalityName(OUT unsigned char ** ppUTF8LocalityName ,IN int nIndex)
{

    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);

    if(nIndex == -1 || nIndex < 0 || nIndex >= nNum)
    {
        return false;
    }
    X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, nIndex);

    ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
    ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);
    ASN1_STRING_to_UTF8(ppUTF8LocalityName,str);
    return true;
}
bool Cert::Get_OrganizationalUnitName(OUT unsigned char ** ppUTF8OrganizationalUnitName ,IN int nIndex)
{

    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);

    if(nIndex == -1 || nIndex < 0 || nIndex >= nNum)
    {
        return false;
    }
    X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, nIndex);

    ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
    ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);
    ASN1_STRING_to_UTF8(ppUTF8OrganizationalUnitName,str);
    return true;
}
bool Cert::Get_OrganizationName(OUT unsigned char ** ppUTF8OrganizationName ,IN int nIndex)
{

    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);

    if(nIndex == -1 || nIndex < 0 || nIndex >= nNum)
    {
        return false;
    }
    X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, nIndex);

    ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
    ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);
    ASN1_STRING_to_UTF8(ppUTF8OrganizationName,str);
    return true;
}
bool Cert::Get_CommonName(OUT unsigned char ** ppUTF8CommonName ,IN int nIndex)
{

    X509_NAME * Subject = X509_get_subject_name(cert);
    int nNum = X509_NAME_entry_count(Subject);

    if(nIndex == -1 || nIndex < 0 || nIndex >= nNum)
    {
        return false;
    }
    X509_NAME_ENTRY *NameEntry = X509_NAME_get_entry(Subject, nIndex);

    ASN1_OBJECT * object = X509_NAME_ENTRY_get_object(NameEntry);
    ASN1_STRING * str    = X509_NAME_ENTRY_get_data(NameEntry);
    ASN1_STRING_to_UTF8(ppUTF8CommonName,str);
    return true;
}
HashMD Cert::Get_CertHashType()
{
    /*****************************************************************
     *     TODO: Add The Function what  Gain Hash ﻿Algorithm from Cert
     *
     *
     *****************************************************************/
    return eSign::HASH_SHA1;
}
bool Cert::CompareCert(X509 * x509_cert1,X509 * x509_cert2)
{
    bool bIsSame = false;

    if(x509_cert1 == NULL || x509_cert2 == NULL)
    {
        return bIsSame;
    }

    int nResult = X509_cmp(x509_cert1,x509_cert2);
    if(nResult == 0)
    {
        bIsSame = true;
    }

    return bIsSame;
}

bool Cert::LoadCertChain(const char *path)
{
    PKCS7 *p7 = NULL;
    BIO *in = BIO_new(BIO_s_file());
    int der = 1;
    int i;
    chain = sk_X509_new_null();
    CRYPTO_malloc_init();
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    if(BIO_read_filename(in, path)<=0)
        LTHROW(ERR_PKCS_LOADCHAIN_FAILED)
        p7 = der ?d2i_PKCS7_bio(in,NULL) : PEM_read_bio_PKCS7(in, NULL,NULL,NULL);
    i = OBJ_obj2nid(p7->type);
    if(i == NID_pkcs7_signed)
    {
        chain = p7->d.sign->cert;
    }
    else if(i == NID_pkcs7_signedAndEnveloped)
    {
        chain = p7->d.signed_and_enveloped->cert;
    }
    BIO_free(in);
    if(!p7)
    {
        PKCS7_free(p7);
    }
    if(chain)
        return true;
    else
        return false;
}

