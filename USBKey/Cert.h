/** eSign project
 * file : Cert.h
 * author : Lodevil
 */

#ifndef __ESIGN_CERT_HEADER__
#define __ESIGN_CERT_HEADER__

/* Add By Feimyy */
#ifndef IN
#define IN
#endif
#ifndef OUT
#define OUT
#endif

/******************/

#include "global.h"
#include <string>
#include "Hash.h"
#include "CertStore.h"
#include "Device.h"
namespace eSign
{

/* Add By Feimyy */

enum TYPE
{
    CountryName,
    StateOrProvinceName,
    LocalityName,
    OrganizationName,
    OrganizationalUnitName,
    CommonName,
    Pkcs9_emailAddress
};
/*****************/
/** @class Cert
 * @brief 用户证书。
 * 用于解析证书、使用证书验证签名。
 */
class Cert
{
public:
    /** @fn Cert
     * @brief 证书构造函数
     */
    Cert();
    /**
     *@brief 从DER字符串构造证书
     *@param [in] buf DER证书字符串
     */
    Cert(const std::string &buf);
    /**
     *@brief 从DER字符串构造证书
     *@param [in] buf DER证书地址
     *@param [in] length 字符串长度
     */
    Cert(const void *buf,
         unsigned int length);
    /**
     *@brief 从X509构造证书
     *@param [in] x509 X509证书
     */
    Cert(X509 *x509);
    ~Cert();
    /**
     *@brief 加载DER证书
     *@param [in] buf DER证书地址
     *@param [in] length DER证书长度
     */
    void Load(const void *buf,
              unsigned int length);
    /**
     *@brief 加载PEM证书
     *@param [in] path cer file path
     */
    void LoadFile(const char *path);
    /**
     *@brief 验证证书的有效性
     *@return 证书是否有效
     *@warning 调用此函数前必须先调用LoadCertChain
     *@exception 该函数可能会抛出下列异常
     *@par
     * - ERR_PKCS_LOADCHAIN_UNINIT 根证书链未初始化
     */
    bool VerifyCert();
    /**
     *@brief 获取证书公钥
     *@return EVP_PKEY公钥
     */
    EVP_PKEY *Pubkey() const;
    /**
     *@brief 验证初始化
     *@param [in] md Hash方法
     */
    void VerifyInit(HashMD md);
    /**
     *@brief 验证更新
     *@param [in] buf 更新的数组
     *@param [in] length 更新的长度
     */
    void VerifyUpdate(const void *buf,
                      unsigned int length);
    /**
     *@brief 验证更新
     *@param [in] buf 更新的字符串
     */
    void VerifyUpdate(const std::string &buf);
    /**
     *@brief 验证结束
     *@param [in] sig 签名数据
     *@param [in] length 签名数据的长度
     *@return 验证是否通过
     */
    bool VerifyFinal(const void *sig,
                     unsigned int length);
    /**
     *@brief 验证结束
     *@param [in] sig 签名数据
     *@return 验证是否通过
     */
    bool VerifyFinal(const std::string &sig);
    /**
     *@brief 验证数据签名
     *@param [in] md Hash类型
     *@param [in] buf 字符串地址
     *@param [in] buf_len 字符串长度
     *@param [in] sig 签名数据
     *@param [in] sig_len 签名数据长度
     *@return 是否通过验证
     */
    bool Verify(HashMD md, const void *buf, unsigned int buf_len,
                const void *sig, unsigned int sig_len);
    /**
     *@brief 验证数据签名
     *@param [in] md Hash类型
     *@param [in] buf 字符串地址
     *@param [in] sig 签名数据
     *@return 是否通过验证
     */
    bool Verify(HashMD md, const std::string &buf, const std::string &sig);
    /**
     *@brief 证书版本
     *@return 版本号
     */
    int version();
    /**
     *@brief 获取发行者
     *@return X509_NAME发行者
     */
    const X509_NAME* issuer();
    /**
     *@brief 获取证书主题
     *@return X509_NAME主题
     */
    const X509_NAME* subject();
    /**
     *@brief get cert
     *@return cert
     */
    X509* get_x509();

    /**
     *  @brief  获取X509证书序列号
     *  @param  [in,out] X509证书的序列号
     *  @param    [in,out] 序列号的长度
     *  @author 2014/4/15 by wutao
     *  @warning 序列号为二进制数据，非字符串数据。
     */
    void Get_SerialNumber(OUT unsigned char * pSerialNumber, size_t &len);

    /**
     *  @brief  获取序列号长度
     *  @author 2014/4/15 by wutao
     *  @return 返回序列号长度
     *  @deprecated 该函数已经被弃用，长度直接由Get_SerialNumber参数返回
     */
    int Get_SerialNumberLength();

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到x509证书的生效时间
     *  Param  [OUT]  UTF8编码后的生效时间
     *
     ************************************************************************/
    void Get_CertValidDate(OUT unsigned char ** ppUTF8ValidDate);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到生效时间字符串的长度
     *  return :      UTF8编码后的生效时间的长度
     *
     ************************************************************************/
    int Get_ValidDateLength();

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到x509证书的失效时间
     *  Param  [OUT]  UTF8编码后的失效时间
     *
     ************************************************************************/
    void Get_CertInvalidDate(OUT unsigned char ** ppUTF8InvalidDate);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到失效时间字符串的长度
     *  return :      UTF8编码后的失效时间的长度
     *
     ************************************************************************/
    int Get_InvalidDateLength();
    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到der编码的公钥
     *  Param  [OUT]  der编码的的公钥
     *  Param  [in]   EVP_PKEY公钥
     *
     ************************************************************************/

    void Get_derPubKey(OUT unsigned char * pderPubKey,IN EVP_PKEY * pPubKey);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到der编码的公钥的长度
     *  Param  :[IN]  EVP_PKey公钥
     *  return :      der编码的公钥的长度
     *
     ************************************************************************/
    int Get_derPubKeyLength(IN EVP_PKEY * pPubKey);

    /***********************************************************************
    *
    *  Author :      Feimyy
    *  Brief  :      得到发行者信息
    *  Param  [OUT]  der编码的的公钥
    *  Param  [in]   条目索引
    *  Param  [OUT]  信息类型
    ************************************************************************/

    void Get_CertSujectInfo(OUT unsigned char ** ppSubjectInfo,IN int nIndex,OUT TYPE * enumType);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到发行者的信息条目数
     *  return :      发行者的信息条目数
     *
     ************************************************************************/
    int Get_CertSubjectNum();

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到每个条目子符长度
     *  Param  [in]   条目索引
     *  return :      每个条目子符长度
     *
     ************************************************************************/
    int Get_CertPreSubjectLength(IN int nIndex);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到主题单个项的索引
     *  Param  [in]   要获取的enum类型
     *  return :      项索引,如果不存在,返回-1
     *
     ************************************************************************/
    int Get_IndexOfSubject(IN TYPE enumType);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到主题单个项的参赌
     *  Param  [in]   要获取的enum类型
     *  return :      项长度,如果不存在,返回0
     *
     ************************************************************************/
    int Get_LengthOfSubject(IN TYPE enumType);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到主题中国家的类型
     *  Param  [OUT]  国家类型
     *  Param  [in]   索引
     *  return :      是否获取成功，成功 true,失败 false
     *
     ************************************************************************/
    bool  Get_CoutryName(OUT unsigned char ** ppUTF8CoutryName ,IN int nIndex);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到主题中省或者州的名称
     *  Param  [OUT]  省或者州的名称
     *  Param  [in]   索引
     *  return :      是否获取成功，成功 true,失败 false
     *
     ************************************************************************/
    bool  Get_StateOrProvinceName(OUT unsigned char ** ppUTF8StateOrProvinceName ,IN int nIndex);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到主题中地区的名称
     *  Param  [OUT]  地区的名称
     *  Param  [in]   索引
     *  return :      是否获取成功，成功 true,失败 false
     *
     ************************************************************************/
    bool  Get_LocalityName(OUT unsigned char ** ppUTF8LocalityName ,IN int nIndex);
    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到主题中的公司名或组织名
     *  Param  [OUT]  公司名或组织名
     *  Param  [in]   索引
     *  return :      是否获取成功，成功 true,失败 false
     *
     ************************************************************************/
    bool  Get_OrganizationName(OUT unsigned char ** ppUTF8OrganizationName ,IN int nIndex);
    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到主题中的OrganizationalUnitName
     *  Param  [OUT]  OrganizationalUnitName
     *  Param  [in]   索引
     *  return :      是否获取成功，成功 true,失败 false
     *
     ************************************************************************/
    bool  Get_OrganizationalUnitName(OUT unsigned char ** ppUTF8OrganizationalUnitName ,IN int nIndex);

    /***********************************************************************
     *
     *  Author :      Feimyy
     *  Brief  :      得到主题中的作用域(CommonName)
     *  Param  [OUT]  作用域(CommonName)
     *  Param  [in]   索引
     *  return :      是否获取成功，成功 true,失败 false
     *
     ************************************************************************/
    bool  Get_CommonName(OUT unsigned char ** ppUTF8CommonName ,IN int nIndex);

    /***********************************************************************
    *
    *  Author :      Feimyy
    *  Brief  :      得到签名采用hash的算法
    *  return :      采用的hash算法，MD5为HASH_MD5,SHA1为HASH_SHA1，
    *                SHA256为HASH_SHA256,SHA512为HASH_SHA512
    *
    ************************************************************************/
    HashMD Get_CertHashType();

    /***********************************************************************
    *
    *  Author :      Feimyy
    *  Brief  :      比较证书
    *  Param  [in]   x509格式的证书
    *  Param  [in]   x509格式的证书
    *  return :      验证的结果，true:证书相同 false:证书不相同
    *
    *
    ************************************************************************/
    bool CompareCert( X509 * x509_cert1,X509 * x509_cert2);

    /**
    *  @brief  载入根证书链
    *  @param [in] path 代表根证书链的路径
    *  @author 2014/4/15 by wutao
    *  @return 是否载入成功
    */
    bool LoadCertChain(const char *path);
private:
    X509 *cert;
    EVP_MD_CTX cert_verify_md;
    STACK_OF(X509) *chain; /** @brief  根证书链条*/
};

}

#endif
