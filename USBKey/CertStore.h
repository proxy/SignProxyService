/* eSign project
 * file : CertStore.h
 * author : Lodevil
 */

#ifndef __ESIGN_CERT_STORE_HEADER__
#define __ESIGN_CERT_STORE_HEADER__

#include "global.h"
#include "CertCRL.h"

namespace eSign {

class Cert;

/** @class CertStore
 * @brief cert store helpers
 * for cert chain
 */
class CertStore {
public:
    CertStore() {
        _store = X509_STORE_new();
    }

    ~CertStore() {
        if (_store) {
            X509_STORE_free(_store);
        }
    }
    /**
        *@brief add X509 cert
        *@param [in] x509 cert
    */
    void AddCert(X509 *x509);
    /**
        *@brief add cert
        *@param [in] cert cert
    */
    void AddCert(Cert &cert);
    /**
        *@brief add crl
        *@param [in] crl CRL
    */
    void AddCRL(CertCRL &crl);
    /**
        *@brief set to check crl
    */
    void SetCheckCRL();
    X509_STORE *get_store();
private:
    X509_STORE *_store;
};

};

#endif
