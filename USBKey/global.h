/* eSign project
 * file : global.h
 * author : Lodevil
 */

#ifndef __ESIGN_GLOBAL_HEADER__
#define __ESIGN_GLOBAL_HEADER__

#ifdef _MSC_VER
    #define _CRT_SECURE_NO_WARNINGS
#endif

#include "Err.h"
#include <openssl/buffer.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/conf.h>
#include <openssl/bio.h>
#include <openssl/objects.h>
#include <openssl/asn1.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/pkcs12.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
 #include <openssl/ssl.h>

void inline openssl_init() {
    OpenSSL_add_all_algorithms();
    SSLeay_add_ssl_algorithms();
    SSL_load_error_strings();
}

#endif
