/* eSign project
 * file : Err.h
 * author : Lodevil
 */

#ifndef __ESIGN_ERR_HEADER__
#define __ESIGN_ERR_HEADER__

#include <exception>
namespace eSign {

enum ESIGN_ERR {
    ///**< enum 无错*/
    ERR_NOERR = 0,
    
    // Hash Errors*/
    ///**< enum *_CTX 初始化失败*/
    ERR_HASH_INIT, 
    ///**< enum hash更新数据失败*/
    ERR_HASH_UPDATE, 
    ///**< enum hash结果失败*/
    ERR_HASH_FINAL, 
    ///**< enum 无效的Hash类型*/
    ERR_HASH_INVALID_MD, 
    
    // Cert Errors*/
    ///**< enum 加载证书失败*/
    ERR_CERT_LOAD, 
    ///**< enum 证书是无效的*/
    ERR_CERT_INVALID, 
    ///**< enum 获取公钥失败*/
    ERR_CERT_PUBKEY, 
    ///**< enum 公钥验证初始化失败*/
    ERR_CERT_VERIFY_INIT, 
    ///**< enum 公钥验证更新失败*/
    ERR_CERT_VERIFY_UPDATE, 
    ///**< enum 公钥验证结果失败*/
    ERR_CERT_VERIFY_FINAL,
    ///**< enum CRL load fail
    ERR_CERT_CRL_LOAD,
    ///**< enum cert store add cert fail
    ERR_CERT_STORE_ADD_CERT,
    ///**< enum cert store add crl fail
    ERR_CERT_STORE_ADD_CRL,
    
    // Device Errors*/
    ///**< enum 设备初始化失败*/
    ERR_DEVICE_INIT, 
    ///**< enum 设别没有初始化*/
    ERR_DEVICE_UNINITED, 
    ///**< enum 获取证书失败*/
    ERR_DEVICE_CERT, 
    ///**< enum 设备签名失败*/
    ERR_DEVICE_SIGN, 
    ///**< enum 设别加密失败*/
    ERR_DEVICE_ENCRYPT, 
    ///**< enum 设备解密失败*/
    ERR_DEVICE_DECRYPT, 
    
    // SSL Socket Errors*/
    ///**< enum [Win]WSAStartup失败*/
    ERR_WSA_STARTUP, 
    ///**< enum [Win]socket类型错误*/
    ERR_SOCKET_VERSION, 
    ///**< enum socket创建失败*/
    ERR_SOCKET, 
    ///**< enum socket连接失败*/
    ERR_SOCKET_CONN, 
    ///**< enum SSL_CTX_new失败*/
    ERR_SSL_CTX, 
    ///**< enum SSL_new失败*/
    ERR_SSL_NEW, 
    ///**< enum SSL_connect失败*/
    ERR_SSL_CONN, 
    ///**< enum SSL_read错误*/
    ERR_SSL_READ, 
    ///**< enum SSL_write错误*/
    ERR_SSL_WRITE, 
    
    // EVP Errors*/
    ///**< enum evp初始化失败*/
    ERR_EVP_INIT, 
    ///**< enum evp更新失败*/
    ERR_EVP_UPDATE, 
    ///**< enum evp结果失败*/
    ERR_EVP_FINAL, 
    
    // Seal Errors*/
    ///**< enum 印章不完整*/
    ERR_SEAL_INCOMPLETE, 
    ///**< enum 印章无效*/
    ERR_SEAL_INVALID, 
    ///**< enum 印章序列化失败*/
    ERR_SEAL_SERIALIZE, 
    ///**< enum 印章解析失败*/
    ERR_SEAL_PARSE, 
    ///**< enum 印章列表序列化失败*/
    ERR_SEALLIST_SERIALIZE, 
    ///**< enum 印章列表解析失败*/
    ERR_SEALLIST_PARSE, 
    
    // SignatureMsg Errors*/
    ///**< enum 签章信息不完整*/
    ERR_MSG_INCOMPLETE, 
    ///**< enum 需要文档*/
    ERR_MSG_NEED_DOC, 
    ///**< enum 签章信息序列化失败*/
    ERR_MSG_SERIALIZE, 
    ///**< enum DS_HASH无效*/
    ERR_MSG_DS_INVALID, 
    
    // Signature Errors*/
    ///**< enum 文档无效*/
    ERR_SIG_DOC_INVALID, 
    ///**< enum 前一个签章无效*/
    ERR_SIG_PRESIG_INVALID, 
    ///**< enum 签章序列化失败*/
    ERR_SIG_SERIALIZE, 
    ///**< enum 需要先验证文档*/
    ERR_SIG_NEED_DOC, 
    ///**< enum 没有任何签章*/
    ERR_SIG_NOITEM, 
    ///**< enum 签章Item序列化失败*/
    ERR_SIGITEM_SERIALIZE, 
    ///**< enum 签章Item不完整*/
    ERR_SIGITEM_INCOMPLETE, 
    ///**< enum 签章Item无效*/
    ERR_SIGITEM_INVALID, 
    
    // User Errors*/
    ///**< enum 没有这个设备*/
    ERR_USER_NOSUCH_DEVICE, 
    ///**< enum 用户没有初始化*/
    ERR_USER_UNINITED, 
    ///**< enum 必须先连接到服务器*/
    ERR_USER_UNCONNECTED, 
    ///**< enum 必须先登录*/
    ERR_USER_UNLOGIN, 
    ///**< enum 登录失败*/
    ERR_USER_LOGIN, 
    ///**< enum 发送消息失败*/
    ERR_USER_SEND, 
    ///**< enum 接受消息失败*/
    ERR_USER_RECV, 
    ///**< enum 获取印章列表失败*/
    ERR_USER_GETSEALLIST, 
    ///**< enum 获取印章失败*/
    ERR_USER_GETSEAL, 
    ///**< enum 数据无效*/
    ERR_USER_SERVER_DATA_INVALID, 
    
    ///**< enum 未知错误*/
    ERR_UNKNOWN, 
    ///**< enum BIO错误*/
    ERR_BIO,
	///**< enum USBKey验证错误*/
	ERR_DEVICE_VERIFY,
	
	///* Add by feimyy*/
	
	///* 没有USBKEY */
	ERR_PKCS_NODEVICE,
	///* 载入PKCS11库失败 */
	ERR_PKCS_LOAD_LIB,
	///* 得到PKCS11库函数列表错误 */
	ERR_PKCS_GET_FUNCTIONLIST,
	///* 初始化错误*/
	ERR_PKCS_INITIALIZE,
	///* 没有发现签名私钥 */
	ERR_PKCS_FIND_PRIVATE,
	///* 没有发现签名证书 */
	ERR_PKCS_NO_SIGN_CERTIFICATE,
	///* 签名初始化错误 */
	ERR_PKCS_SIGNINIT,
	///* 签名错误 */
	ERR_PKCS_SIGN,
	///* 得到签名证书错误 */
	ERR_PKCS_GET_CERTIFICATE,
	///* USBKey密码错误 */
	ERR_PKCS_PASSWORD_INVALID,
	///* 打开USBKey失败 */
	ERR_PKCS_LOGIN_USBKEY_FAILED,
	/**<  根证书链未初始化 */
	ERR_PKCS_LOADCHAIN_UNINIT,
	/**<  根证书载入失败 */
	ERR_PKCS_LOADROOT_FAILED,
	/**<  根证书链载入失败 */
	ERR_PKCS_LOADCHAIN_FAILED,
};

/** @class Err
 * @brief 异常。
 * 在使用接口时可能会抛出异常。
 */
class Err:public std::exception {
    public:
        Err(int errcode, const char *file = 0, int line = -1);
        virtual const char *what() const throw();
        /**
         *@brief 获取错误编码
         *@return 错误编码
         */
        int code() const;
    private:
        int errcode;
        int line;
        const char *file;
};

#ifdef DEBUG
    /** eSign throw*/
    #define LTHROW(err) throw Err(err, (const char*)__LINE__, (int)__FILE__);
#else
    /** eSign throw*/
    #define LTHROW(err) throw Err(err);
#endif

}
#endif

