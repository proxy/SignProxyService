/* eSign project
 * file : Device.cpp
 * author : Lodevil
 */

#include "Device.h"
#include "Hash.h"
#include <string.h>
#include <openssl/sha.h>
#include <openssl/md5.h>
using namespace eSign;

Device::Device()
{
    is_ok = false;
    x509_str = NULL;
}

Device::~Device()
{
    if(x509_str)
        delete x509_str;
}

void Device::Initialize(const char *name, const char *password)
{
    strncpy(device, name, MAX_DEVICE_LEN);
    device[MAX_DEVICE_LEN - 1] = 0;
}

void Device::GetX509(X509 **x) const
{
    LTHROW(ERR_DEVICE_CERT)
}

void Device::GetX509(const X509 **x) const
{
    LTHROW(ERR_DEVICE_CERT)
}

void Device::GetX509(unsigned char *buf, unsigned int &length) const
{
    const X509* x;
    GetX509(&x);
    
    length = i2d_X509(const_cast<X509*>(x), &buf);
    if(length <= 0)
        LTHROW(ERR_DEVICE_CERT)
}

const std::string* Device::GetX509()
{
    static unsigned char buf[2048];
    unsigned int length;
    
    if(!x509_str){
        GetX509(buf, length);
        x509_str = new std::string((char*)buf, length);
    }
    
    return x509_str;
}

void Device::Sign(RSA_SIGN_TYPE type, const unsigned char *digest,
                unsigned int d_len, unsigned char *sigret,
                unsigned int &siglen)
{
    LTHROW(ERR_DEVICE_SIGN)
}
void Device::SignData(RSA_SIGN_TYPE type, const void *msg,
                unsigned int m_len, unsigned char *sigret,
                unsigned int &siglen) 
{
	unsigned char digest[64];
	int d_len;

	switch(type){
	case SIGN_SHA1:
		Hash::SHA1(msg, m_len, digest);
		d_len = 20;
		break;
	case SIGN_MD5:
		Hash::MD5(msg, m_len, digest);
		d_len = 16;
		break;
	default:
		LTHROW(ERR_HASH_INVALID_MD)
	}

	Sign(type, digest, d_len, sigret, siglen);
}

bool Device::IsOK() const
{
    return is_ok;
}

