/* eSign project
 * file : Hash.h
 * author : Lodevil
 */

#ifndef __ESIGN_HASH_HEADER__
#define __ESIGN_HASH_HEADER__

#include "global.h"
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <string>

namespace eSign {
enum HashMD {
    HASH_INVALID = -1,
    HASH_MD5 = 0,
    HASH_SHA1 = 1,
    HASH_SHA256 = 2,
    HASH_SHA512 = 3
};

const unsigned int HASH_MD5_length = 16;
const unsigned int HASH_SHA1_length = 20;
const unsigned int HASH_SHA256_length = 32;
const unsigned int HASH_SHA512_length = 64;

const unsigned int HASH_SIZES[] = {16, 20, 32, 64};
inline unsigned int hash_size(int md)
{
    if(md < 0 || md > sizeof(HashMD) - 2)
        return -1;
    return HASH_SIZES[md];
}

/** @class Hash
 * @brief Hash工具
 * 计算常见Hash
 */
class Hash{
    public:
        /**
         *@brief MD5字符串加密
         *@param [in] data 加密的字符串
         *@param [in] data_len 字符串长度
         *@param [out] hashret 结果hash值
         */
        static void MD5(const void *data, unsigned int data_len,
                    unsigned char *hashret);
        /**
         *@brief SHA1字符串加密
         *@param [in] data 加密的字符串
         *@param [in] data_len 字符串长度
         *@param [out] hashret 结果hash值
         */
        static void SHA1(const void *data, unsigned int data_len,
                    unsigned char *hashret);
        /**
         *@brief SHA256字符串加密
         *@param [in] data 加密的字符串
         *@param [in] data_len 字符串长度
         *@param [out] hashret 结果hash值
         */
        static void SHA256(const void *data, unsigned int data_len,
                    unsigned char *hashret);
        /**
         *@brief SHA512字符串加密
         *@param [in] data 加密的字符串
         *@param [in] data_len 字符串长度
         *@param [out] hashret 结果hash值
         */
        static void SHA512(const void *data, unsigned int data_len,
                    unsigned char *hashret);
        /**
         *@brief 计算字符串Hash
         *@param [in] type Hash类型
         *@param [in] data 加密的字符串
         *@param [in] data_len 字符串长度
         *@param [out] hashret 结果hash值
         */
        static void CalcHash(HashMD type, const void *data,
                    unsigned int data_len, unsigned char *hashret);
        /**
         *@brief 构造函数
         */
        Hash();
        /**
         *@brief 获取当前的Hash类型
         */
        HashMD GetMD() const;
        
        /**
         *@brief 初始化Hash计算
         *@param [in] md Hash类型
         */
        void Init(HashMD md);
        /**
         *@brief 更新Hash
         *@param [in] buf 更新的字符串
         *@param [in] length 字符串长度
         */
        void Update(const void *buf, unsigned int length);
        /**
         *@brief 获取Hash值
         *@param [out] hashret Hash结果
         */
        void Final(unsigned char *hashret);
        
        /**
         *@brief 更新Hash
         */
        friend inline Hash& operator>>(Hash &hash, std::string &str){
            hash.Update(str.c_str(), str.size());
            return hash;
        }
        /**
         *@brief 获取Hash结果
         */
        friend inline Hash& operator<<(Hash &hash, std::string &str){
            unsigned char digest[64];
            
            str.clear();
            hash.Final(digest);
            str = std::string((char*)digest, hash_size(hash.GetMD()));
            return hash;
        }
        /**
         *@brief 获取Hash结果
         */
        friend inline Hash& operator<<(Hash &hash, void* buf){
            hash.Final((unsigned char*)buf);
            return hash;
        }
    private:
        HashMD md;
        MD5_CTX md5_ctx;
        SHA_CTX sha1_ctx;
        SHA256_CTX sha256_ctx;
        SHA512_CTX sha512_ctx;
};
}




#endif


