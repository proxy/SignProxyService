/* eSign project
 * file : Hash.cpp
 * author : Lodevil
 */

#include "Hash.h"
using namespace eSign;

#define HASH_FUNC(MD) \
    MD##_CTX ctx; \
    if(MD##_Init(&ctx) != 1) \
        LTHROW(ERR_HASH_INIT) \
    if(MD##_Update(&ctx, data, data_len) != 1) \
        LTHROW(ERR_HASH_UPDATE) \
    if(MD##_Final(hashret, &ctx) != 1) \
        LTHROW(ERR_HASH_FINAL) \

void Hash::MD5(const void *data, unsigned int data_len,
        unsigned char *hashret)
{
    HASH_FUNC(MD5)
}

void Hash::SHA1(const void *data, unsigned int data_len,
        unsigned char *hashret)
{
    SHA_CTX ctx;
    if(SHA1_Init(&ctx) != 1)
        LTHROW(ERR_HASH_INIT)
    if(SHA1_Update(&ctx, data, data_len) != 1)
        LTHROW(ERR_HASH_UPDATE)
    if(SHA1_Final(hashret, &ctx) != 1)
        LTHROW(ERR_HASH_FINAL)
}

void Hash::SHA256(const void *data, unsigned int data_len,
        unsigned char *hashret)
{
    HASH_FUNC(SHA256)
}

void Hash::SHA512(const void *data, unsigned int data_len,
        unsigned char *hashret)
{
    HASH_FUNC(SHA512)
}

void Hash::CalcHash(HashMD type, const void *data,
        unsigned int data_len, unsigned char *hashret)
{
    switch(type){
        case HASH_MD5:
            return MD5(data, data_len, hashret);
        case HASH_SHA1:
            return SHA1(data, data_len, hashret);
        case HASH_SHA256:
            return SHA256(data, data_len, hashret);
        case HASH_SHA512:
            return SHA512(data, data_len, hashret);
        default:
            LTHROW(ERR_HASH_INVALID_MD)
    }
}

Hash::Hash()
{

}

HashMD Hash::GetMD() const
{
    return md;
}

#define MKSURE_RETONE(fun, SUF)\
    if(fun != 1)\
        LTHROW(ERR_HASH_##SUF)\
    break;
void Hash::Init(HashMD md)
{
    if(md < 0 || md > sizeof(HashMD) - 2)
        LTHROW(ERR_HASH_INVALID_MD)
    this->md = md;
    switch(md){
        case HASH_MD5:
            MKSURE_RETONE(MD5_Init(&md5_ctx), INIT)
        case HASH_SHA1:
            MKSURE_RETONE(SHA1_Init(&sha1_ctx), INIT)
        case HASH_SHA256:
            MKSURE_RETONE(SHA256_Init(&sha256_ctx), INIT)
        case HASH_SHA512:
            MKSURE_RETONE(SHA512_Init(&sha512_ctx), INIT)
        default:
            LTHROW(ERR_HASH_INVALID_MD)
    }
}

void Hash::Update(const void *buf, unsigned int length)
{
    switch(md){
        case HASH_MD5:
            MKSURE_RETONE(MD5_Update(&md5_ctx, buf, length), UPDATE)
        case HASH_SHA1:
            MKSURE_RETONE(SHA1_Update(&sha1_ctx, buf, length), UPDATE)
        case HASH_SHA256:
            MKSURE_RETONE(SHA256_Update(&sha256_ctx, buf, length), UPDATE)
        case HASH_SHA512:
            MKSURE_RETONE(SHA512_Update(&sha512_ctx, buf, length), UPDATE)
        default:
            LTHROW(ERR_HASH_INVALID_MD)
    }
}

void Hash::Final(unsigned char *hashret)
{
    switch(md){
        case HASH_MD5:
            if(MD5_Final(hashret, &md5_ctx) != 1)
                LTHROW(ERR_HASH_FINAL)
            break;
        case HASH_SHA1:
            if(SHA1_Final(hashret, &sha1_ctx) != 1)
                LTHROW(ERR_HASH_FINAL)
            break;
        case HASH_SHA256:
            if(SHA256_Final(hashret, &sha256_ctx) != 1)
                LTHROW(ERR_HASH_FINAL)
            break;
        case HASH_SHA512:
            if(SHA512_Final(hashret, &sha512_ctx) != 1)
                LTHROW(ERR_HASH_FINAL)
            break;
        default:
            LTHROW(ERR_HASH_INVALID_MD)
    }
}








