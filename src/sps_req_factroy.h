/** sps_req_factroy.h - header file for sps_req_factroy.cpp
 */

#ifndef __SPS_REQ_FACTROY_H__
#define __SPS_REQ_FACTROY_H__

#include "sps_config.h"

class SpsReqFactroy
{
 public:
  /** @brief Load the request message from server.
   *  @param msg [in] The message from server.
   *  @param ip [in] The local ip address.
   *  @param mac [in] The local mac address.
   *  @return This function always returns a valid pointer.
   */
  SpsReq* loadRequest(const std::string& msg,
		      const std::string& ip,
		      const std::string& mac);
};

#endif /* __SPS_REQ_FACTROY_H__ */
