/** sps_req.h - header file for sps_req.cpp
 * 
 * This file defines the SpsReq structure for request message. The SpsReq 
 * structure contains the most common data that all requests have.
 *
 */


#ifndef __SPS_REQ_H__
#define __SPS_REQ_H__

#include "sps_config.h"

#ifndef _SPS_JSON_NAME_
#define _SPS_JSON_NAME_

#define SJN_TYPE                     "type"
#define SJN_CODE                     "code"
#define SJN_SESSION_ID               "sessionId"
#define SJN_AGENT_CLIENT_IP          "agentClientIp"
#define SJN_ALGORITHM                "algorithm"
#define SJN_SIGNATURE                "signature"
#define SJN_SEND_DATA                "sendData"

#define SJN_RESULT_STATUS_CODE       "resultStatusCode"
#define SJN_RESULT_CODE              "resultCode"
#define SJN_RESULT_DESC              "resultDesc"
#define SJN_RESULT_DATA              "resultData"

#endif /* _SPS_JSON_NAME_ */


#ifndef _SPS_ERROR_CODE_
#define _SPS_ERROR_CODE_

#define ESPS_SUCCESS                 0      /**< 成功 */
#define ESPS_INTEGRITY               1      /**< 数据完整性校验失败 */
#define ESPS_NO_USBKEY               2      /**< USBKEY不存在 */
#define ESPS_INVALID_USBKEY          3      /**< 无效的USBKEY */
#define ESPS_PARSE_FAILED            4	    /**< JSON数据解析失败 */
#define ESPS_INVALID_HASH            5	    /**< 未知哈希函数 */
#define ESPS_INVALID_OPERATION       6	    /**< 请求类型错误 */
#define ESPS_UNKNOWN_ERROR           7	    /**< 未知错误 */

#endif /*  _SPS_ERROR_CODE_ */

class SpsReq
{
 public:
  SpsReq(const std::string& ip, const std::string& mac);
  ~SpsReq();

  /** @brief Handle the request message.
   */
  virtual void handleRequest();

  /** @brief Get the response message.
   *  @return The response msessage in json format.
   *  @note If you add other request message, you might need to 
   *   rewrite this function.
   */
  virtual std::string getResponse();

  void set_err(int e)
  {
    err_ = e;
  }

  void set_req(const json& r)
  {
    req_ = json(r);
  }

 protected:

  /** @brief Convert the string to utf-8 string.
   *  @param str [in] The string to be converted.
   *  @return Returns the utf-8 encode string.
   */
  static std::string str2utf8(const char *str);

  /** @brief Get the hash of the *msg*.
   *  @return The hash string of the *msg*.
   */
  std::string getHash(const std::string& msg);

  /** @brief Check the request whether is integrity.
   *  @param msg [in] The message to be verified.
   *  @return If no errors, ESPS_SUCCESS is returned. If hash type error,
   *   ESPS_INVALID_HAHS is returned. Otherwise, -1 is returned.
   */
  int checkRequest(const std::string& msg);

  /** @brief Get the result code.
   */
  int getResultCode();

  /** @brief Get the result status code.
   */
  std::string getResultStatusCode();

  /** @brief Get the result decription.
   *  @note The description string adopts utf-8 encoding.
   */
  std::string getResultDesc();

  /** @brief Load the request.
   *  @return if no errors, ESPS_SUCCESS is returned.
   *  @note For each request, you should rewrite this function.
   */
  virtual int load();

  /** @brief Verify the request.
   *  @return If no errors, ESPS_SUCCESS is returned.
   *  @note You should rewrite this function in subclass.
   */
  virtual int verify();

  /** @brief Do core work.
   *  @return If no error, ESPS_SUCCESS is returned.
   *  @note For each request, you should rewrite this function.
   */
  virtual int doit();

  /** @brief Generate the response verify information.
   */
  virtual std::string generateVerify(const json& v);
  

  std::string local_ip_;
  std::string local_mac_;
  int         err_;

  int         type_;
  int         code_;
  std::string session_;
  std::string agentip_;
  std::string msgalgo_;
  std::string msgsign_;

  json        req_;
};


extern std::string sps_priv_key;

#endif /* __SPS_REQ_H__ */
