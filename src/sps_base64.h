/** sps_base64.h - header file for sps_base64.c
 * 
 * This file includes the implemention about base64 encode and decode.
 *
 */


#ifndef __SPS_BASE64_H__
#define __SPS_BASE64_H__

#include "sps_config.h"

/** @brief Encode the string with base64.
 *  @param str [in] The string to be encoded.
 *  @return A string encoded with base64 is returned.
 */
std::string Base64Encode(const std::string& str);

/** @brief Decode the string with base64.
 *  @param str [in] The string encoded with base64.
 *  @return A plain string is returned.
 */
std::string Base64Decode(const std::string& str);

#endif /*  __SPS_BASE64_H__ */
