/** sps_auth_req.h - header file for sps_auth_req.cpp
 */

#ifndef __SPS_AUTH_REQ_H__
#define __SPS_AUTH_REQ_H__

#include "sps_config.h"

class SpsAuthReq : public SpsReq
{
 public:
  SpsAuthReq(const std::string& ip, const std::string& mac);
  ~SpsAuthReq();

  virtual std::string getResponse();

 protected:

  /** @brief Load the initAuthCode rquest.
   */
  virtual int load();

  /** @brief Verify the initAuthCode request.
   */
  virtual int verify();

  /** @brief Get initAuthCode response.
   */
  virtual int doit();

  virtual std::string generateVerify(const json& v);

 private:
  /** @brief Update the auth information.
   *  @return If no errors, 0 is returned. Otherwise, -1 is returned.
   */
  int updateAuthInfo();
  
  unsigned long     start_date_;
  unsigned long     end_date_;
  int               userid_;
  int               state_;
  int               usemode_;
  int               usesitu_;
  int               useperm_;
  std::string       usever_;
  std::string       id_;
  std::string       apikey_;
  std::string       skey_;
  std::string       tskey_;
};

#endif /*  __SPS_AUTH_REQ_H__ */
