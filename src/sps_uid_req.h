/** sps_uid_req.h - header file for sps_uid_req.cpp
 * 
 * This file includes the GET_USBKEY_ID request declaration.
 */

#ifndef __SPS_UID_REQ_H__
#define __SPS_UID_REQ_H__

#include "sps_config.h"

class SpsUidReq : public SpsReq
{
 public:
  SpsUidReq(const std::string& ip, const std::string& mac);
  ~SpsUidReq();

  /** @brief Get the response message of getting usbkey id reqeuest.
   */
  virtual std::string getResponse();

 protected:

  /** @brief Do core work for getting usbkey id.
   */
  virtual int doit();

  /** @brief Generate the response verify information.
   */
  virtual std::string generateVerify(const json& v);

 private:
  std::string devid_;
  std::string pubcert_;
};

#endif /* __SPS_UID_REQ_H__ */
