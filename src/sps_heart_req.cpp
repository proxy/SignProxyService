/** sps_heart_req.cpp - implemention of the heartbeat detection
 */

#include "sps_heart_req.h"

#define SJN_RANDOM          "random"

SpsHeartReq::SpsHeartReq(const std::string& ip, const std::string& mac)
  : SpsReq(ip, mac)
  , random_(-1)
{
}

SpsHeartReq::~SpsHeartReq()
{
}

std::string SpsHeartReq::getResponse()
{
  json v;

  v[SJN_TYPE] = 1;
  v[SJN_CODE] = code_;

  v[SJN_RANDOM] = random_;

  return v.to_string();
}


/*******************************************************************************
 * Protected functions                                                         *
 ******************************************************************************/

int SpsHeartReq::load()
{
  try {
    code_ = req_[SJN_CODE].as_int();
    random_ = req_[SJN_RANDOM].as_int();
  } catch (const json_exception& e) {
    SpsLog::instance().logError("load request from json failed (%s)", e.what());
    /* NOTE: Should response error for heartbeat? */
    //    return ESPS_PARSE_FAILED;
  }

  return ESPS_SUCCESS;
}

int SpsHeartReq::verify()
{
  /* NOTE: The heartbeat do not need to verify. */
  return ESPS_SUCCESS;
}

int SpsHeartReq::doit()
{
  random_ += 1;

  return ESPS_SUCCESS;
}
