/** sps_heart_req.h - header file for sps_heart_req.cpp
 */

#ifndef __SPS_HEART_REQ_H__
#define __SPS_HEART_REQ_H__

#include "sps_config.h"

class SpsHeartReq : public SpsReq
{
 public:
  SpsHeartReq(const std::string& ip, const std::string& mac);
  ~SpsHeartReq();

  /** @brief Get response message about heartbeat request.
   */
  virtual std::string getResponse();

 protected:

  /** @brief Load the heartbeat request.
   */
  virtual int load();

  /** @brief Verify the heartbeat request.
   */
  virtual int verify();

  /** @brief Do heartbeat response work.
   */
  virtual int doit();

 private:
  int random_;
};

#endif /* __SPS_HEART_REQ_H__ */
