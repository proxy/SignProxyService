/** sps_req.cpp - request structure implemention
 */

#include "sps_config.h"

typedef struct _res_s
{
  char *status;
  char *desc;
  int   code;
} res_t;

#ifdef _SPS_CHINESE_

static res_t sps_res[] = {
  /**< ESPS_SUCCESS */
  { "100210000", "操作成功", 0 },

  /**< ESPS_INTEGRITY */
  { "100210100", "数据不完整", 1 },

  /**< ESPS_NO_USBKEY */
  { "100210101", "USBKEY不存在", 1 },

  /**< ESPS_INVALID_USBKEY */
  { "100210102", "USBKEY未生效", 1 },

  /**< ESPS_PARSE_FAILED */
  { "100210200", "数据解析或处理错误", 1 },

  /**< ESPS_INVALID_HASH */
  { "100210203", "未知哈希函数", 1 },

  /**< ESPS_INVALID_OPERATION */
  { "100210201", "请求类型错误", 1 },

  /**< ESPS_UNKNOWN_ERROR */
  { "100210105", "未预测到的错误或异常", 1 },
  
  { NULL, NULL, 1 }
};

#else

static res_t sps_res[] = {
  /**< ESPS_SUCCESS */
  { "100210000", "operation is successful", 0 },
  
  /**< ESPS_INTEGRITY */
  { "100210100", "data is incomplete", 1 },
  
  /**< ESPS_NO_USBKEY */
  { "100210101", "usbkey is not exist", 1 },
  
  /**< ESPS_INVALID_USBKEY */
  { "100210102", "usbkey is invalid", 1 },
  
  /**< ESPS_PARSE_FAILED */
  { "100210200", "data parsing or handling errors", 1 },
  
  /**< ESPS_INVALID_HASH */
  { "100210203", "unknown hash function", 1 },
  
  /**< ESPS_INVALID_OPERATION */
  { "100210201", "request type error", 1 },
  
  /**< ESPS_UNKNOWN_ERROR */
  { "100210105", "do not predict error or exception", 1 },
  
  { NULL, NULL, 1 }
};

#endif


/* The private key used to check message.
 * This variable used by all request message except for heartbeat request,
 * is initialized at initAuthCode operation.
 */
std::string sps_priv_key;


SpsReq::SpsReq(const std::string& ip, const std::string& mac)
  : local_ip_(ip)
  , local_mac_(mac)
  , msgalgo_("md5")
{
  
}

SpsReq::~SpsReq()
{
  
}

void SpsReq::handleRequest()
{
  err_ = load();
  if (err_ != ESPS_SUCCESS) {
    return;
  }

  err_ = verify();
  if (err_ != ESPS_SUCCESS) {
    return;
  }

  err_ = doit();
}

std::string SpsReq::getResponse()
{
  json v;
  
  v[SJN_TYPE] = 1;
  v[SJN_CODE] = code_;
  v[SJN_RESULT_STATUS_CODE] = getResultStatusCode();
  v[SJN_RESULT_CODE] = getResultCode();
  v[SJN_RESULT_DESC] = getResultDesc();
  v[SJN_SESSION_ID] = session_;
  v[SJN_AGENT_CLIENT_IP] = local_ip_;
  v[SJN_ALGORITHM] = msgalgo_;

  v[SJN_SIGNATURE] = SpsReq::generateVerify(v);

  return v.to_string();
}


/*******************************************************************************
 * Protected functions                                                         *
 ******************************************************************************/

std::string SpsReq::str2utf8(const char *str)
{
  size_t   len;
  wchar_t *ws;
  char    *s;

  len = MultiByteToWideChar(CP_ACP, 0, str, -1, NULL, 0);

  ws = new wchar_t[len + 1];
  memset(ws, 0, len + 1);
  MultiByteToWideChar(CP_ACP, 0, str, -1, ws, len);

  len = WideCharToMultiByte(CP_UTF8, 0, ws, -1, NULL, 0, NULL, NULL);
  s = new char[len + 1];

  WideCharToMultiByte(CP_UTF8, 0, ws, -1, s, len, NULL, NULL);
  std::string stmp(str);

  delete [] ws;
  delete [] s;
  
  return stmp;
}

std::string SpsReq::getHash(const std::string& msg)
{
 wrapperfactory  f;
  hashwrapper    *hash;
  std::string     sig;
  
  try {
    hash = f.create(msgalgo_);
    sig = hash->getHashFromString(msg);
    delete hash;
  } catch (hlException& e) {
    SpsLog::instance().logError("get hash error (%s)",
				e.error_message().c_str());
  }

  return sig;
}

int SpsReq::checkRequest(const std::string& msg)
{
  wrapperfactory  f;
  hashwrapper    *hash;
  std::string     sig;
  
  try {
    hash = f.create(msgalgo_);
    sig = hash->getHashFromString(msg);
    delete hash;
  } catch (hlException& e) {
    SpsLog::instance().logError("get hash error (%s)",
				e.error_message().c_str());

    /* The receive hash type is not support by sign proxy service. 
     * Use the default hash type instead.
     */
    msgalgo_ = "md5";
    return ESPS_INVALID_HASH;
  }

  return sig == msgsign_ ? ESPS_SUCCESS : ESPS_INTEGRITY;
}

int SpsReq::getResultCode()
{
  return sps_res[err_].code;
}

std::string SpsReq::getResultStatusCode()
{
  return sps_res[err_].status;
}

std::string SpsReq::getResultDesc()
{
#ifdef _SPS_CHINESE_
  return str2utf8(sps_res[err_].desc);
#else
  return sps_res[err_].desc;
#endif
}

int SpsReq::load()
{
  try {
    type_ = req_[SJN_TYPE].as_int();
    code_ = req_[SJN_CODE].as_int();
    session_ = req_[SJN_SESSION_ID].as_string();
    agentip_ = req_[SJN_AGENT_CLIENT_IP].as_string();
    msgalgo_ = req_[SJN_ALGORITHM].as_string();
    msgsign_ = req_[SJN_SIGNATURE].as_string();
  } catch (const json_exception& e) {
    SpsLog::instance().logError("load request from json failed (%s)", e.what());
    return ESPS_PARSE_FAILED;
  }
  
  return ESPS_SUCCESS;
}

int SpsReq::verify()
{
  std::list<std::string> l;

  l.push_back(req_[SJN_TYPE].as_string());
  l.push_back(req_[SJN_CODE].as_string());
  l.push_back(req_[SJN_SESSION_ID].as_string());
  l.push_back(req_[SJN_AGENT_CLIENT_IP].as_string());
  l.push_back(req_[SJN_ALGORITHM].as_string());


  l.push_back(sps_priv_key);

  l.sort();

  std::string msg;
  std::list<std::string>::const_iterator i;
  for (i = l.begin(); i != l.end(); i++) {
    msg += (*i + "&");
  }

  msg.erase(msg.end() - 1);
  
  return checkRequest(msg);
}

int SpsReq::doit()
{
  return ESPS_SUCCESS;
}

std::string SpsReq::generateVerify(const json& v)
{
  std::list<std::string> l;

  l.push_back(v[SJN_TYPE].as_string());
  l.push_back(v[SJN_CODE].as_string());
  l.push_back(v[SJN_RESULT_STATUS_CODE].as_string());
  l.push_back(v[SJN_RESULT_CODE].as_string());
  l.push_back(v[SJN_RESULT_DESC].as_string());
  l.push_back(v[SJN_SESSION_ID].as_string());
  l.push_back(v[SJN_AGENT_CLIENT_IP].as_string());
  l.push_back(v[SJN_ALGORITHM].as_string());

  l.push_back(sps_priv_key);

  l.sort();

  std::string msg;
  std::list<std::string>::const_iterator i;
  for (i = l.begin(); i != l.end(); i++) {
    msg += (*i + "&");
  }

  msg.erase(msg.end() - 1);

  return getHash(msg);
}
