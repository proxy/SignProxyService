#include "sps_config.h"

std::string str2utf8(const char *str)
{
  char        *s;
  size_t       len;
  wchar_t     *ws;
  std::string  tmp;

  len = MultiByteToWideChar(CP_ACP, 0, str, -1, NULL, 0);

  ws = new wchar_t[len + 1];
  memset(ws, 0, len + 1);
  MultiByteToWideChar(CP_ACP, 0, str, -1, ws, len);

  len = WideCharToMultiByte(CP_UTF8, 0, ws, -1, NULL, 0, NULL, NULL);
  s = new char[len + 1];

  WideCharToMultiByte(CP_UTF8, 0, ws, -1, s, len, NULL, NULL);
  tmp = std::string(s, len + 1);

  delete [] s;
  delete [] ws;

  return tmp;
}
