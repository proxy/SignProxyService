/** sps_exist_req.h - header file for sps_exist_req.cpp
 */

#ifndef __SPS_EXIST_REQ_H__
#define __SPS_EXIST_REQ_H__

#include "sps_config.h"

class SpsExistReq : public SpsReq
{
 public:
  SpsExistReq(const std::string& ip, const std::string& mac);
  ~SpsExistReq();

  /** @brief Get the response message of checking whether usbkey exist.
   */
  virtual std::string getResponse();


 protected:

  /** @brief Do core work for checking whether usbkey exist.
   */
  virtual int doit();

  /** @brief Generate the response verify information.
   */
  virtual std::string generateVerify(const json& v);

 private:
  bool exist_;
};

#endif /* __SPS_EXIST_REQ_H__ */
