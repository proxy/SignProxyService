/** sps_log.h - header file for sps_log.h
 *
 * This file includes the class of SpsLog defination.
 *
 */
 

#ifndef __SPS_LOG_H__
#define __SPS_LOG_H__

#include "sps_config.h"

class SpsLog
{
public:
  static SpsLog& instance();

  bool openLog(const char *path);
  void closeLog();

  void logEmerg(const char *fmt, ...);
  void logAlert(const char *fmt, ...);
  void logCrit(const char *fmt, ...);
  void logError(const char *fmt, ...);
  void logWarn(const char *fmt, ...);
  void logInfo(const char *fmt, ...);
  void logDebug(const char *fmt, ...);

  void logStderr(const char *fmt, ...);
	
private:
  SpsLog();
  ~SpsLog();
  SpsLog(const SpsLog& rhs);
  SpsLog& operator=(const SpsLog& rhs);

  enum {
    LOG_EMERG = 0,
    LOG_ALERT,
    LOG_CRIT,
    LOG_ERROR,
    LOG_WARN,
    LOG_INFO,
    LOG_DEBUG
  };

  static void onCreate();
  static void onDeadReference();

  void logCore(int level, const char *fmt, va_list args);

  static SpsLog   *log_;
  static FILE     *file_;
  static bool      destroyed_;	
};

#endif /* _SPS_LOG_H__ */
