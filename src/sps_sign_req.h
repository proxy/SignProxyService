/** sps_sign_req.h - header file for sps_sign_req.cpp
 */

#ifndef __SPS_SIGN_REQ_H__
#define __SPS_SIGN_REQ_H__

#include "sps_config.h"

class SpsSignReq : public SpsReq
{
 public:
  SpsSignReq(const std::string& ip, const std::string& mac);
  ~SpsSignReq();

  virtual std::string getResponse();

 protected:

  /** @brief Load the use usbkey sign request.
   */
  virtual int load();

  /** @brief Verify the ues usbkey sign request.
   */
  virtual int verify();

  /** @brief Do core work for using usbkey sign request.
   */
  virtual int doit();

  /** @brief Generate the response verify information.
   */
  virtual std::string generateVerify(const json& v);

 private:
  std::string   pin_;
  std::string   serial_;
  std::string   hashdata_;
  unsigned long hashdate_;
  unsigned long estimated_;

  std::string   signalgo_;
  std::string   signdata_;
  unsigned long signdate_;
};

#endif /* __SPS_SIGN_REQ_H__ */
