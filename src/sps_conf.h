/** sps_conf.h - header file for sps_conf.cpp
 *
 * This file includes the declaration of the configure file class.
 */


#ifndef __SPS_CONF_H__
#define __SPS_CONF_H__

#include "sps_config.h"

class SpsConf
{
 public:
  static SpsConf& instance();

  /** @brief Load configure file.
   *  @param file [in] The configure file name.
   *  @param If no errors, zero is returned. Otherwise, -1 is returned.
   */
  int loadConf(const char *file);

  const char *get_host();  
  const char *get_serv();
  unsigned int get_timeout_wait_num();
  unsigned long get_reconnect_time_sec();  
  unsigned long get_reconnect_time_usec();  
  unsigned long get_connect_timeout_sec();  
  unsigned long get_connect_timeout_usec();
   
 private:
  SpsConf();
  ~SpsConf();
  SpsConf(const SpsConf& rhs);
  SpsConf& operator=(const SpsConf& rhs);

  static void onCreate();
  static void onDeadReference();

  /** @brief Load configure from string.
   *  @param buf [in] The buffer of the configure string.
   *  @param len [in] The length of the configure string.
   */
  int loadConfString(const char *buf, size_t len);

  static SpsConf *conf_;
  static bool     destroyed_;
  std::string     host_;
  std::string     serv_;
  unsigned int    timeout_wait_num_;	
  unsigned long   reconnect_time_sec_;	
  unsigned long   reconnect_time_usec_;	
  unsigned long   connect_timeout_sec_;	
  unsigned long   connect_timeout_usec_;

};

#endif /* __SPS_CONF_H__ */
