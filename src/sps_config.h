/** sps_config.h - header file for SignProxyService project
 *
 * This file contains all headers which are used by this project.
 *
 */
 
#ifndef __SPS_CONFIG_H__
#define __SPS_CONFIG_H__
#include <winsock2.h>
#include <WS2tcpip.h>
#include <IPHlpApi.h>
#include <windows.h>

#include <iostream>
#include <string>
#include <list>
#include <direct.h>

#include <event2/event.h>
#include <event2/event_struct.h>

#include <jsoncons/json.hpp>
using jsoncons::json;
using jsoncons::json_exception;

#include <hashlibpp.h>

#include "global.h"
#include "USBKey.h"
#include "Cert.h"
using namespace eSign;

#define getpid     GetCurrentProcessId
#define gettid     GetCurrentThreadId

#define SPS_SERVICE_NAME   TEXT("Sign Proxy Service")

#if (_MSC_VER >= 1500)

#define O_RDWR       _SH_DENYNO
#define O_READ       _SH_DENYRD
#define O_WRITE      _SH_DENYWR

#define fopen(path, mode, access)     _fsopen(path, mode, access)

#else

#define O_RDWR       0
#define O_READ       0
#define O_WRITE      0

#define fopen(path, mode, access)     fopen(path, mode)

#endif

#if (_MSC_VER < 1900)
#define snprintf        _snprintf
#endif

/** If defined _SPS_CHINESE_, the responsed message's result
 *  description is Chinese. Otherwise, using English. 
 */
#define _SPS_CHINESE_    0


extern std::string sps_db_name;

#include "sqlite3.h"
#include "sps_utils.h"
#include "sps_base64.h"
#include "sps_log.h"
#include "sps_conf.h"
#include "sps_req.h"
#include "sps_uid_req.h"
#include "sps_auth_req.h"
#include "sps_sign_req.h"
#include "sps_exist_req.h"
#include "sps_valid_req.h"
#include "sps_heart_req.h"
#include "sps_req_factroy.h"
#include "sps_service.h"
 
#endif /* __SPS_CONFIG_H__ */
