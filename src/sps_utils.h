#ifndef __SPS_UTILS_H__
#define __SPS_UTILS_H__

#include "sps_config.h"

/** @brief Convert the string to utf-8 string.
 *  @param str [in] The string to be converted.
 *  @return utf-8 encode string is returned.
 */
std::string str2utf8(const char *str);

#endif /* __SPS_UTILS_H__ */
