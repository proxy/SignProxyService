/** sps_valid_req.h - header file for sps_valid_req.cpp
 */

#ifndef __SPS_VALID_REQ_H__
#define __SPS_VALID_REQ_H__

#include "sps_config.h"

class SpsValidReq : public SpsReq
{
 public:
  SpsValidReq(const std::string& ip, const std::string& mac);
  ~SpsValidReq();

  /** @brief Get the response message of checking whether usbkey exist.
   */
  virtual std::string getResponse();

 protected:

  /** @brief Do core work for checking whether usbkey is valid.
   */
  virtual int doit();

  /** @brief Generate response message verify information.
   */
  virtual std::string generateVerify(const json& v);

 private:
  bool valid_;
};

#endif /* __SPS_VALID_REQ_H__ */
